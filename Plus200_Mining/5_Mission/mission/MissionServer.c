modded class MissionServer
{
	P200MiningConfig m_P200MiningConfig;
	override void OnInit()
	{
		super.OnInit();

		this.m_P200MiningConfig = P200MiningConfig.Cast(GetPlugin(P200MiningConfig));
		this.m_P200MiningConfig.LoadConfig();
	}

    override void InvokeOnConnect(PlayerBase player, PlayerIdentity identity)
    {
        super.InvokeOnConnect(player, identity);
        P200MiningConfig test = this.m_P200MiningConfig;
        this.m_P200MiningConfig.sendToClient();
    }
}
