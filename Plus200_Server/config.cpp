
class CfgPatches
{
	class Plus200_Server
	{
		units[]={
		    "InfectedSkin"
		};
		weapons[]={};
		requiredVersion=0.1;
        requiredAddons[]=
            {
                "DZ_Scripts",
                "DZ_Data",
                "DZ_Characters",
                "DZ_gear_food",
                "DZ_Characters_Vests",
                "DZ_Gear_Consumables",
                "DZ_Characters_Tops",
                "Plus200_Recipes",
                "Plus200_Home"
            };
	};
};

class CfgMods
{
	class Plus200_Server_1
	{
	    dir = "Plus200_Server";
	    picture = "";
	    action = "";
	    hideName = 1;
	    hidePicture = 1;
	    name = "Plus200";
	    credits = "Plus200";
	    author = "Plus200";
	    authorID = "76561198078441547";
	    version = "0.1";
	    extra = 0;
	    type = "mod";

	    dependencies[] = {"World"};

	    class defs
	    {
			class worldScriptModule
            {
                value = "";
                files[] = {"Plus200_Server/4_World"};
            };
        };
    };
};

class CfgVehicles
{
    class PigPelt;
    class InfectedSkin: PigPelt
    {
        scope=2;
        displayName="Infected Skin";
        descriptionShort="Skin from an infected";
        model="\dz\gear\consumables\Pelt_Pig.p3d";
        peltGain=6;
        weight=420;
        itemSize[]={5,3};
    };
    class DZ_LightAI;
	class DayZInfected: DZ_LightAI
	{
	};
	class ZombieBase: DayZInfected
	{
		class Skinning
		{
			class Clothing
			{
				item="InfectedSkin";
				count=1;
				quantityMinMaxCoef[]={0.2,1};
			};
			class Lard
            {
                item="Lard";
                count=1;
                quantityMinMaxCoef[]={0.2,1};
            };
			class Steaks
			{
				item="HumanSteakMeat";
				count=1;
				quantityMinMaxCoef[]={0.2,1};
			};
			class Guts
			{
				item="Guts";
				count=1;
				quantityMinMaxCoef[]={0.2,1};
			};
			class Bones
			{
				item="Bone";
				count=1;
				quantityMinMaxCoef[]={0.2,0.6};
			};
		};
	};
}
