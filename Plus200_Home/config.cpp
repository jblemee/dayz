
class CfgPatches
{
	class Plus200_Home
	{
		units[]={};
		weapons[]={};
		requiredVersion=0.1;
        requiredAddons[]=
            {
                "DZ_Data"
            };
	};
};

class CfgMods
{
	class Plus200_Home_1
	{
	    dir = "Plus200_Home";
	    picture = "";
	    action = "";
	    hideName = 1;
	    hidePicture = 1;
	    name = "Plus200";
	    credits = "Plus200";
	    author = "Plus200";
	    authorID = "76561198078441547";
	    version = "0.2";
	    extra = 0;
	    type = "mod";

	    dependencies[] = {"World"};

	    class defs
	    {
			class worldScriptModule
            {
                value = "";
                files[] = {"Plus200_Home/4_World"};
            };
        };
    };
};
