class CAContinuousMineStatic : CAContinuousMineWood
{
    P200MiningConfig miningConfig;

	void CAContinuousMineStatic(float time_between_drops)
	{
		m_TimeBetweenMaterialDrops = time_between_drops;

		if (GetGame().IsServer()){
            miningConfig = P200MiningConfig.Cast(GetPlugin(P200MiningConfig));
        }
	}

    void GetMaterialAndQuantityMap(string targetType, out map<string,int> output_map)
    {
        if (GetGame().IsServer()){
            output_map.Clear();
            ref array<ref P200ItemProbability> miningItemProbability = miningConfig.getBuildingProbabilities(targetType);
            for(int i = 0; i < miningItemProbability.Count(); i++) {
                P200ItemProbability prob = miningItemProbability.Get(i);
                int proba = prob.probability;
                while(proba > 0) {
                    int r = Math.RandomInt(0,100);
                    if(r <= proba) {
                        output_map.Set(prob.item, output_map.Get(prob.item) + 1);
						Print("Should Pop");
                    }
                    proba -= 100;
                }
            }
        }
    }
	
	override bool GetMiningData(ActionData action_data )
	{
		Building ntarget;
		if ( Class.CastTo(ntarget, action_data.m_Target.GetObject()) )
		{
			m_AmountOfDrops = Math.Max(1,GetAmountOfDrops(action_data.m_MainItem));
			//m_Material = ntarget.GetMaterial(action_data.m_MainItem);
			//m_AmountOfMaterialPerDrop = Math.Max(1,ntarget.GetAmountOfMaterialPerDrop(action_data.m_MainItem));
			ref P200MinableItemConfig config = miningConfig.getItemConfig(ntarget.GetType());
			int minedCount = miningConfig.mineCount.Get(ntarget.ToString());
            if(config.max_mine == -1 || config.max_mine > minedCount ) {
                GetMaterialAndQuantityMap(ntarget.GetType(), m_MaterialAndQuantityMap);
            }
			m_DamageToMiningItemEachDrop = GetDamageToMiningItemEachDrop(action_data.m_MainItem);
			m_AdjustedDamageToMiningItemEachDrop = action_data.m_Player.GetSoftSkillsManager().SubtractSpecialtyBonus( m_DamageToMiningItemEachDrop, m_Action.GetSpecialtyWeight(), true );
			return true;
		}
		return false;
	}

    override void CreatePrimaryItems(ActionData action_data)
    {
        super.CreatePrimaryItems(action_data);
        EntityAI ntarget;
        if ( Class.CastTo(ntarget, action_data.m_Target.GetObject()) )
        {
            ref P200MinableItemConfig config = miningConfig.getItemConfig(ntarget.GetType());
            int minedCount = miningConfig.mineCount.Get(ntarget.ToString());
            if(config.max_mine == -1 || config.max_mine > minedCount ) {
                GetMaterialAndQuantityMap(ntarget.GetType(), m_MaterialAndQuantityMap);
            } else {
                m_MaterialAndQuantityMap.Clear();
            }
        }
    }

    int GetAmountOfDrops(ItemBase item)
    {
        return 1;
    }

    float GetDamageToMiningItemEachDrop(ItemBase item)
    {
        return 10;
    }
};