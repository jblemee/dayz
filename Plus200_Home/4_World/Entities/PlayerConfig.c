class Plus200PlayerHomeConfig
{
    static const string PluginFolder = "$profile:\\Plus200\\";
    static const string FileFolder = PluginFolder + "Home\\";

    private string m_FilePath;
    private ref map< string, string > params = new map< string, string >;

	void Plus200PlayerHomeConfig(string playerId)
	{
		if( GetGame().IsServer() && playerId)
		{
		    Plus200HomeConfig.CheckFolder();
            m_FilePath = FileFolder + playerId + ".json";
            this.GetOrCreateConfigFile();
			
			if(!this.HomeAround(this.GetParam( "home_point")))
			{
			    this.params["home_point"] = string.Empty;
                this.Save();
			}
		}
	}

	private bool HomeAround(string homePoint)
	{
		if(homePoint != string.Empty)
		{
			ref array<Object> nearest_objects = new array<Object>;
			ref array<CargoBase> cargo_objects = new array<CargoBase>;
			GetGame().GetObjectsAtPosition3D( homePoint.ToVector(), 3, nearest_objects, cargo_objects );
			
			for ( int i = 0; i < nearest_objects.Count(); ++i )
			{
				Object object = nearest_objects.Get( i );
				Print("[PLUS200] object.ClassName() ="+ object.ClassName());
				Print("[PLUS200] Plus200HomeConfig.GetItemClassName() ="+ Plus200HomeConfig.GetItemClassName());
				if ( GetGame().IsKindOf( object.ClassName(), Plus200HomeConfig.GetItemClassName()))
				{
					return true;
				}
			}

		}

		return false;
	}

	private void GetOrCreateConfigFile()
    {
        if(FileExist(m_FilePath))
        {
            ref Plus200PlayerHomeConfig config = this;
            JsonFileLoader<Plus200PlayerHomeConfig>.JsonLoadFile(m_FilePath, config);
        }
        else
        {
            this.params["home_point"] = string.Empty;
            this.Save();
        }
    }

     ref map< string, string > Get()
	{
		return this.params;
	}

	typename HomeObject()
	{
	    return ItemBase;
	}
	
	void SetHome(vector position)
	{
	    this.SetParam("home_point", position[0].ToString() + " " + position[1].ToString() + " " + position[2].ToString());
	}

	vector GetStartPosition(vector orElse) {
	    if(this.params["home_point"])
	    {
	        return this.params["home_point"].ToVector();
	    }
	    else
	    {
	        return orElse;
	    }
	}

    private void Save()
    {
        JsonFileLoader<Plus200PlayerHomeConfig>.JsonSaveFile(m_FilePath, this);
    }

    string GetParam( string key )
    {
        if(params.Count() <= 0)
            return string.Empty;

        if(params[key])
        {
            return params[key];
        }
        else
        {
            return string.Empty;
        }
    }

	void SetParam(string key, string value)
	{
		params[key] = value;
        this.Save();
	}

    static void OnPlacementComplete( Man player, vector position = "0 0 0", vector orientation = "0 0 0" )
    {
        if ( GetGame().IsServer() )
        {
            PlayerBase player_base = PlayerBase.Cast( player );

            ref Plus200PlayerHomeConfig config = new Plus200PlayerHomeConfig(player_base.GetIdentity().GetPlainId());
            config.SetHome(position);
        }
    }
}

