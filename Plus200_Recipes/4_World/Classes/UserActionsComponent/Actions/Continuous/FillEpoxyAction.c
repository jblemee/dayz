class FillEpoxyActionCB : ActionContinuousBaseCB
{
	override void CreateActionComponent()
	{
		m_ActionData.m_ActionComponent = new CAContinuousTime(UATimeSpent.RESTRAIN);
	}
};

class ActionFillEpoxy : ActionContinuousBase
{
	void ActionFillEpoxy()
	{
		m_CallbackClass = FillEpoxyActionCB;
		m_CommandUID = DayZPlayerConstants.CMD_ACTIONFB_CUTBARK;
        m_StanceMask = DayZPlayerConstants.STANCEMASK_ERECT | DayZPlayerConstants.STANCEMASK_CROUCH;
		m_SpecialtyWeight = UASoftSkillsWeight.ROUGH_LOW;

		m_FullBody = true;
		m_LockTargetOnUse = true;
	}

	override void CreateConditionComponents()
	{
        m_ConditionTarget = new CCTTree(UAMaxDistances.DEFAULT);
        m_ConditionItem = new CCINonRuined;
	}

	override string GetText()
	{
		return "#fill_with_sap";
	}

//	override bool HasProgress()
//	{
//		return true;
//	}
//
//	override bool ActionConditionContinue(ActionData action_data)
//	{
//		return true;
//	}
//
//	override bool HasAlternativeInterrupt()
//	{
//		return true;
//	}

	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		if ( GetGame().IsMultiplayer() && GetGame().IsServer() )
				return true;

		Object targetObject = target.GetObject();

		if ( targetObject.IsTree() && item.IsInherited(EpoxyPutty) && item.GetQuantity() < 100)
		{
			return true;
		}

		return false;
	}

	override void OnFinishProgressServer(ActionData action_data)
	{
		ItemBase m_epoxy = ItemBase.Cast(action_data.m_MainItem);
        m_epoxy.AddQuantity(50, false);
        action_data.m_Player.GetSoftSkillsManager().AddSpecialty(m_SpecialtyWeight);
	}
};