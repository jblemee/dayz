modded class RockBase: Object
{
    P200MiningConfig miningConfig;

	void RockBase()
	{
		if (GetGame().IsServer()){
			miningConfig = P200MiningConfig.Cast(GetPlugin(P200MiningConfig));
		}
	}

	override void GetMaterialAndQuantityMap(ItemBase item, out map<string,int> output_map)
	{
		if (GetGame().IsServer()){
	        ref map<string, int> miningItemProbability = miningConfig.getProbabilities();
	        for(int i = 0; i < miningItemProbability.Count(); i++) {
	            string key = miningItemProbability.GetKey(i);
	            int proba = miningItemProbability.GetElement(i);
				while(proba > 0) {
					int r = Math.RandomInt(0,100);
					if(r <= proba) {
		                output_map.Set(key, output_map.Get(key) + 1);
		            }
					proba -= 100;
				}
	        }
		}
	}
};