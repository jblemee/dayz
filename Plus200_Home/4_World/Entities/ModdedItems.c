modded class ActionDropItem
{
    override void OnExecuteServer( ActionData action_data )
    {
        super.OnExecuteServer(action_data);
//        Print("[PLUS200]  action_data.m_MainItem.ClassName() ="+  action_data.m_MainItem.ClassName());
//        Print("[PLUS200] Plus200HomeConfig.GetItemClassName() ="+ Plus200HomeConfig.GetItemClassName());

        if (GetGame().IsKindOf(  action_data.m_MainItem.ClassName(), Plus200HomeConfig.GetItemClassName()))
        {
           Plus200PlayerHomeConfig.OnPlacementComplete(action_data.m_Player, action_data.m_MainItem.GetPosition());
        }
    }
};

modded class ItemBase
{
    override void EOnInit(IEntity other, int extra)
  	{
  	    super.EOnInit(other, extra);
  		if( !GetGame().IsMultiplayer() || GetGame().IsServer() )
		{
            if (GetGame().IsKindOf( this.ClassName(), Plus200HomeConfig.GetItemClassName())) {
//                Print("[PLUS200] this.ClassName()) ="+ this.ClassName());
//                Print("[PLUS200] Plus200HomeConfig.GetItemClassName() ="+ Plus200HomeConfig.GetItemClassName());

                ref array<Man> players = new array<Man>;
                GetGame().GetPlayers( players );
                PlayerBase closestPlayer;
                for ( int i = 0; i < players.Count(); i++ )
                {
                    PlayerBase player;
                    PlayerIdentity player_id;
                    Class.CastTo(player, players.Get(i));
                    if( player ) {
                        if(player.IsAlive()) {
                            if(! closestPlayer) {
                                closestPlayer = player;
                            } else {
                                if( vector.Distance(this.GetPosition(), player.GetPosition()) <  vector.Distance(this.GetPosition(), closestPlayer.GetPosition()))
                                {
                                    closestPlayer = player;
                                }
                            }
                        }
                    }
                }

                if(closestPlayer) {
                    Plus200PlayerHomeConfig.OnPlacementComplete(closestPlayer, this.GetPosition());
                }
            }
		}
    }

    override void OnPlacementComplete( Man player, vector position = "0 0 0", vector orientation = "0 0 0" )
    {
        if (!GetGame().IsMultiplayer() || GetGame().IsServer())
		{
//            Print("[PLUS200] this.ClassName()) ="+ this.ClassName());
//            Print("[PLUS200] Plus200HomeConfig.GetItemClassName() ="+ Plus200HomeConfig.GetItemClassName());
            super.OnPlacementComplete( player, position, orientation );
            if (GetGame().IsKindOf( this.ClassName(), Plus200HomeConfig.GetItemClassName()))
            {
                Plus200PlayerHomeConfig.OnPlacementComplete(player, GetPosition(), orientation);
            }
		}
    }
};