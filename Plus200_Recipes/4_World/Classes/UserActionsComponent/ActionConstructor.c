modded class ActionConstructor
{
    override void RegisterActions(TTypenameArray actions)
    {
        super.RegisterActions(actions);
		actions.Insert(ActionFillEpoxy);
		actions.Insert(P200ActionRepairCarPart);
		actions.Insert(P200ActionRepairCarRadiator);
		actions.Insert(P200ActionRepairVehiculeBattery);
    }
}