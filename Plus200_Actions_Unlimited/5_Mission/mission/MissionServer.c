modded class MissionServer
{
	P200ActionsUnlimitedConfig m_P200ActionsUnlimitedConfig;
	override void OnInit()
	{
		super.OnInit();

		this.m_P200ActionsUnlimitedConfig = P200ActionsUnlimitedConfig.Cast(GetPlugin(P200ActionsUnlimitedConfig));
		this.m_P200ActionsUnlimitedConfig.LoadConfig();
	}

    override void InvokeOnConnect(PlayerBase player, PlayerIdentity identity)
    {
        super.InvokeOnConnect(player, identity);
        this.m_P200ActionsUnlimitedConfig.sendToClient();
    }
}
