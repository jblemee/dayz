class CAUnlimited : CAContinuousBase
{
	protected float 				m_TimeElapsed;
	protected float 				m_AdjustedTimeBetweenMaterialDrops;
	protected float 				m_CycleTimeOverride;
	protected float 				m_TimeBetweenMaterialDrops;
	protected float					m_DamageToItemEachDrop;
	protected float					m_AdjustedDamageToItemEachDrop;
	protected int 					m_AmountOfDrops;
	protected ref map<string,int> 	m_MaterialAndQuantityMap;
	protected float					m_TimeToComplete;
	protected ref Param1<float>		m_SpentUnits;
	protected bool					m_DataLoaded = false;
	protected const int 			MINEDITEM_MAX = 5;
	protected ItemBase				m_MinedItem[MINEDITEM_MAX];

    protected P200ActionsUnlimitedConfig actionConfig;
    protected P200ActionConfig action;
        
	void CAUnlimited(float time_between_drops)
	{
		m_TimeBetweenMaterialDrops = time_between_drops;
		m_CycleTimeOverride = -1.0;
                                    
        if (GetGame().IsServer()){
            actionConfig = P200ActionsUnlimitedConfig.Cast(GetPlugin(P200ActionsUnlimitedConfig));
        }
	}

	void SetAction(P200ActionConfig action) {
	    this.action = action;
	}

	override void Setup( ActionData action_data )
	{
		m_TimeElapsed = 0;
		if ( !m_SpentUnits )
		{
			m_SpentUnits = new Param1<float>(0);
		}
		else
		{
			m_SpentUnits.param1 = 0;
		}
		m_MaterialAndQuantityMap = new map<string,int>;
		m_DataLoaded = GetDropData(action_data);
		for(int i = 0; i < m_MaterialAndQuantityMap.Count(); i++)
		{
			Print("material = " + m_MaterialAndQuantityMap.GetKey(i) + "; quantity = " + m_MaterialAndQuantityMap.GetElement(i));
		}

		if (m_CycleTimeOverride > -1.0)
		{
			m_TimeBetweenMaterialDrops = m_CycleTimeOverride;
			if (!action_data.m_MainItem) //hand action
				m_TimeBetweenMaterialDrops = m_TimeBetweenMaterialDrops * 3;
		}
		m_AdjustedTimeBetweenMaterialDrops = action_data.m_Player.GetSoftSkillsManager().SubtractSpecialtyBonus( m_TimeBetweenMaterialDrops, m_Action.GetSpecialtyWeight(), true );
		m_TimeToComplete = m_AmountOfDrops * m_AdjustedTimeBetweenMaterialDrops;
	}

	override int Execute( ActionData action_data )
	{
		Object targetObject;
		Class.CastTo(targetObject, action_data.m_Target.GetObject());
		if ( !action_data.m_Player || !m_DataLoaded )
		{
			return UA_ERROR;
		}

		if ( (action_data.m_MainItem && action_data.m_MainItem.IsDamageDestroyed()) || targetObject.IsDamageDestroyed() )
		{
			return UA_FINISHED;
		}
		else
		{
			if ( m_TimeElapsed < m_AdjustedTimeBetweenMaterialDrops )
			{
				m_TimeElapsed += action_data.m_Player.GetDeltaT();
			}
			else
			{
				if ( GetGame().IsServer() )
				{
					float damage = 0;
					if (m_AmountOfDrops > 0)
						damage = (1 / m_AmountOfDrops) * 100;
					targetObject.DecreaseHealth("","",damage,true);
					CreateItems(action_data);
					if (action_data.m_MainItem)
						action_data.m_MainItem.DecreaseHealth( "", "", m_AdjustedDamageToItemEachDrop );
					else
					{
						DamagePlayersHands(action_data.m_Player);
					}
				}
				if ( targetObject.IsDamageDestroyed() )
				{
					if ( m_SpentUnits )
					{
						m_SpentUnits.param1 = m_TimeElapsed;
						SetACData(m_SpentUnits);
					}

					OnCompletePogress(action_data);
					return UA_FINISHED;
				}
				m_TimeElapsed = m_TimeElapsed - m_AdjustedTimeBetweenMaterialDrops;
				OnCompletePogress(action_data);
			}
			return UA_PROCESSING;
		}
	}

	override float GetProgress()
	{
		//float progress = m_TimeElapsed/m_AdjustedTimeBetweenMaterialDrops;
		return m_TimeElapsed/m_AdjustedTimeBetweenMaterialDrops;
	}

    //---------------------------------------------------------------------------


    void GetMaterialAndQuantityMap(EntityAI target, EntityAI tool, out map<string,int> output_map)
    {
        if (GetGame().IsServer()){
            ref array<ref P200DropProbability> dropProbabilities = actionConfig.getDropProbabilities(target, tool);
            for(int i = 0; i < dropProbabilities.Count(); i++) {
                P200DropProbability prob = dropProbabilities.Get(i);
                int proba = prob.probability;
                while(proba > 0) {
                    int r = Math.RandomInt(0,100);
                    if(r <= proba) {
                        output_map.Set(prob.item, output_map.Get(prob.item) + 1);
                    }
                    proba -= 100;
                }
            }
        }
    }
    
    override bool GetDropData(ActionData action_data )
    {
        EntityAI ntarget;
        if ( Class.CastTo(ntarget, action_data.m_Target.GetObject()) )
        {
            m_AmountOfDrops = Math.Max(1,GetAmountOfDrops(action_data.m_MainItem));
            //m_Material = ntarget.GetMaterial(action_data.m_MainItem);
            //m_AmountOfMaterialPerDrop = Math.Max(1,ntarget.GetAmountOfMaterialPerDrop(action_data.m_MainItem));
            GetMaterialAndQuantityMap(ntarget, action_data.m_MainItem, m_MaterialAndQuantityMap);
            m_DamageToItemEachDrop = GetDamageToItemEachDrop(action_data.m_MainItem);
            m_AdjustedDamageToItemEachDrop = action_data.m_Player.GetSoftSkillsManager().SubtractSpecialtyBonus( m_DamageToItemEachDrop, m_Action.GetSpecialtyWeight(), true );
            return true;
        }
        return false;
    }

    int GetAmountOfDrops(ItemBase item)
    {
        return 1;
    }

    float GetDamageToItemEachDrop(ItemBase item)
    {
        return 10;
    }

	void CreateItems(ActionData action_data)
	{
		Object targetObject;
		Class.CastTo(targetObject, action_data.m_Target.GetObject());

		bool correct_pile = true;
		string material;
		int increment;
		for(int i = 0; i < m_MaterialAndQuantityMap.Count(); i++)
		{
			material = m_MaterialAndQuantityMap.GetKey(i);

			if (material != "")
			{
				increment = m_MaterialAndQuantityMap.GetElement(i);

				if ( !m_MinedItem[i] )
				{
					m_MinedItem[i] = ItemBase.Cast(GetGame().CreateObject(material,action_data.m_Player.GetPosition(), false));
					m_MinedItem[i].SetQuantity(increment);
					//Print("CreateItems | first stack");
				}
				else if (m_MinedItem[i].HasQuantity())
				{
					if ( m_MinedItem[i].IsFullQuantity() )
					{
						int stack_max;
						if (m_MinedItem[i].ConfigIsExisting("varStackMax"))
						{
							stack_max = m_MinedItem[i].ConfigGetFloat("varStackMax");
						}
						else
						{
							stack_max = m_MinedItem[i].GetQuantityMax();
						}
						//Print("CreateItems | new stack");
						increment -= stack_max - m_MinedItem[i].GetQuantity();
						if (increment >= 1.0)
						{
							//m_MinedItem[i].SetQuantity(stack_max);
							m_MinedItem[i] = ItemBase.Cast(GetGame().CreateObject(material,action_data.m_Player.GetPosition(), false));
							m_MinedItem[i].SetQuantity(increment,false);
						}
					}
					else
					{
						//Print("CreateItems | adding quantity to: " + (m_MinedItem[i].GetQuantity() + increment));
						m_MinedItem[i].AddQuantity(increment,false);
					}
				}
				else
				{
					m_MinedItem[i] = ItemBase.Cast(GetGame().CreateObject(material,action_data.m_Player.GetPosition(), false));
				}
			}
		}
	}

	void DamagePlayersHands(PlayerBase player)
	{
		//m_AdjustedDamageToItemEachDrop
		ItemBase gloves = ItemBase.Cast(player.FindAttachmentBySlotName("Gloves"));
		if ( gloves && !gloves.IsDamageDestroyed() )
		{
			gloves.DecreaseHealth("","",m_AdjustedDamageToItemEachDrop);
		}
		else
		{
			int rand = Math.RandomIntInclusive(0,9);
			//Print("rand: " + rand);
			if ( rand == 0 )
			{
				rand = Math.RandomIntInclusive(0,1);
				if ( rand == 0 && !player.GetBleedingManagerServer().AttemptAddBleedingSourceBySelection("LeftForeArmRoll") )
				{
					player.GetBleedingManagerServer().AttemptAddBleedingSourceBySelection("RightForeArmRoll");
				}
				else if ( rand == 1 && !player.GetBleedingManagerServer().AttemptAddBleedingSourceBySelection("RightForeArmRoll") )
				{
					player.GetBleedingManagerServer().AttemptAddBleedingSourceBySelection("LeftForeArmRoll");
				}
			}
		}
	}
};