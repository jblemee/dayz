class CfgPatches
{
	class Plus200_Mining
	{
		units[]={};
		weapons[]={};
		requiredVersion=0.1;
		requiredAddons[]=
		{
			"DZ_Data"
		};
	};
};
class CfgMods
{
	class Plus200_Mining_1
	{
		dir="Plus200_Mining";
		picture="";
		action="";
		hideName=1;
		hidePicture=1;
		name="Plus200_Mining";
		credits="Plus200";
		author="Plus200";
		authorID="76561198078441547";
		version="0.1";
		extra=0;
		type="mod";
		dependencies[]=
		{
			"Game",
			"World",
			"Mission"
		};
		class defs
		{
		    class gameScriptModule
            {
                value="";
                files[]=
                {
                    "Plus200_Mining/3_Game"
                };
            };
			class worldScriptModule
			{
				value="";
				files[]=
				{
					"Plus200_Mining/4_world"
				};
			};
			class missionScriptModule
			{
				value="";
				files[]=
				{
					"Plus200_Mining/5_Mission"
				};
			};
		};
	};
};

