modded class MissionServer
{
	P200CraftUnlimitedConfig m_P200CraftUnlimitedConfig;

	override void OnInit()
	{
		super.OnInit();

		this.m_P200CraftUnlimitedConfig = P200CraftUnlimitedConfig.Cast(GetPlugin(P200CraftUnlimitedConfig));
		this.m_P200CraftUnlimitedConfig.LoadConfig();
		PluginRecipesManager recipesManager;
        Class.CastTo(recipesManager, GetPlugin(PluginRecipesManager));

        Print(this.m_P200CraftUnlimitedConfig.recipes.Count());
        for(int i=0; i<this.m_P200CraftUnlimitedConfig.recipes.Count(); i++) {
           recipesManager.RegisterRecipeUnlock(new UnlimitedRecipeBase(this.m_P200CraftUnlimitedConfig.recipes[i]));
        }
        recipesManager.Refresh();
	}

    override void InvokeOnConnect(PlayerBase player, PlayerIdentity identity)
    {
        super.InvokeOnConnect(player, identity);
        this.m_P200CraftUnlimitedConfig.sendToClient();
    }
}
