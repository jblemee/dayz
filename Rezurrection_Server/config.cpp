
class CfgPatches
{
	class Rezurrection_Server
	{
		units[]={
		    "PurgeBillboard_1",
		    "PurgeBillboard_2"
		};
		weapons[]={};
		requiredVersion=0.1;
        requiredAddons[]=
            {
                "Plus200_Recipes",
                "evo_EvocatusSigns",
                "VIRUS"
            };
	};
};

class CfgVehicles
{
    class EvocatusBillboard1Base;
    class EvocatusBillboard2Base;
    class EvocatusBillboard3Base;
    class EvocatusFatBillboard3Base;
    class EvocatusFatBillboard2Base;
    class EvocatusFatBillboard1Base;
    class EvocatusWoodSign1Base;
    class EvocatusWoodSign2Base;
    class EvocatusWoodSign3Base;

    class PurgeBillboard_1: EvocatusBillboard1Base
   	{
   		scope=1;
   		displayName="Purge Billboard 1";
   		hiddenSelectionsTextures[]=
   		{
   			"Rezurrection_Server\data\Custom\purge_zone_1.paa"
   		};
   	};
    class PurgeBillboard_2: EvocatusBillboard1Base
    {
       scope=1;
       displayName="Purge Billboard 2";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\purge_zone_2.paa"
       };
    };
    class PurgeBillboard_3_3: EvocatusFatBillboard3Base
    {
       scope=1;
       displayName="Purge Billboard 3_3";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\purge_zone_3.paa"
       };
    };
    class PurgeBillboard_4_3: EvocatusFatBillboard3Base
    {
       scope=1;
       displayName="Purge Billboard 4_3";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\purge_zone_4.paa"
       };
    };
    class PurgeBillboard_3_2: EvocatusFatBillboard2Base
    {
       scope=1;
       displayName="Purge Billboard 3_2";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\purge_zone_3.paa",
           "Rezurrection_Server\data\Custom\purge_zone_3.paa"
       };
    };
    class PurgeBillboard_4_2: EvocatusFatBillboard2Base
    {
       scope=1;
       displayName="Purge Billboard 4_2";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\purge_zone_4.paa",
           "Rezurrection_Server\data\Custom\purge_zone_4.paa"
       };
    };
    class PurgeBillboard_3_1: EvocatusFatBillboard1Base
    {
       scope=1;
       displayName="Purge Billboard 3_1";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\purge_zone_3.paa"
       };
    };
    class PurgeBillboard_4_1: EvocatusFatBillboard1Base
    {
       scope=1;
       displayName="Purge Billboard 4_1";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\purge_zone_4.paa"
       };
    };

    class RaceFinishBillboard1: EvocatusBillboard1Base
    {
        scope=1;
        displayName="Race Finish Billboard";
        hiddenSelectionsTextures[]=
        {
            "Rezurrection_Server\data\Custom\rezu_race_finish.paa"
        };
    };
    class RaceLeftBillboard1: EvocatusBillboard1Base
    {
       scope=1;
       displayName="Race Left Billboard";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\rezu_race_left.paa"
       };
    };
    class RaceRightBillboard1: EvocatusBillboard1Base
    {
       scope=1;
       displayName="Race Right Billboard";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\rezu_race_right.paa"
       };
    };

    class RaceFinishBillboard2: EvocatusBillboard2Base
    {
        scope=1;
        displayName="Race Finish Billboard 2";
        hiddenSelectionsTextures[]=
        {
            "Rezurrection_Server\data\Custom\rezu_race_finish.paa",
            "Rezurrection_Server\data\Custom\rezu_race_finish.paa"
        };
    };
    class RaceLeftBillboard2: EvocatusBillboard2Base
    {
       scope=1;
       displayName="Race Left Billboard 2";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\rezu_race_left.paa",
           "Rezurrection_Server\data\Custom\rezu_race_left.paa"
       };
    };
    class RaceRightBillboard2: EvocatusBillboard2Base
    {
       scope=1;
       displayName="Race Right Billboard 2";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\rezu_race_right.paa",
           "Rezurrection_Server\data\Custom\rezu_race_right.paa"
       };
    };

    class RaceFinishBillboard3: EvocatusBillboard3Base
    {
        scope=1;
        displayName="Race Finish Billboard 3";
        hiddenSelectionsTextures[]=
        {
            "Rezurrection_Server\data\Custom\rezu_race_finish.paa"
        };
    };
    class RaceLeftBillboard3: EvocatusBillboard3Base
    {
       scope=1;
       displayName="Race Left Billboard 3";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\rezu_race_left.paa"
       };
    };
    class RaceRightBillboard3: EvocatusBillboard3Base
    {
       scope=1;
       displayName="Race Right Billboard 3";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\rezu_race_right.paa"
       };
    };

    class RaceFinishWoodSign1: EvocatusWoodSign1Base
    {
        scope=1;
        displayName="Race Finish Wood Sign";
        hiddenSelectionsTextures[]=
        {
            "Rezurrection_Server\data\Custom\rezu_race_finish.paa"
        };
    };
    class RaceLeftWoodSign1: EvocatusWoodSign1Base
    {
       scope=1;
       displayName="Race Left Wood Sign";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\rezu_race_left.paa"
       };
    };
    class RaceRightWoodSign1: EvocatusWoodSign1Base
    {
       scope=1;
       displayName="Race Right Wood Sign";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\rezu_race_right.paa"
       };
    };

    class RaceFinishWoodSign2: EvocatusWoodSign2Base
    {
        scope=1;
        displayName="Race Finish Wood Sign 2";
        hiddenSelectionsTextures[]=
        {
            "Rezurrection_Server\data\Custom\rezu_race_finish.paa",
            "Rezurrection_Server\data\Custom\rezu_race_finish.paa"
        };
    };
    class RaceLeftWoodSign2: EvocatusWoodSign2Base
    {
       scope=1;
       displayName="Race Left Wood Sign 2";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\rezu_race_left.paa",
           "Rezurrection_Server\data\Custom\rezu_race_left.paa"
       };
    };
    class RaceRightWoodSign2: EvocatusWoodSign2Base
    {
       scope=1;
       displayName="Race Right Wood Sign 2";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\rezu_race_right.paa",
           "Rezurrection_Server\data\Custom\rezu_race_right.paa"
       };
    };

    class RaceFinishWoodSign3: EvocatusWoodSign3Base
    {
        scope=1;
        displayName="Race Finish Wood Sign 3";
        hiddenSelectionsTextures[]=
        {
            "Rezurrection_Server\data\Custom\rezu_race_finish.paa"
        };
    };
    class RaceLeftWoodSign3: EvocatusWoodSign3Base
    {
       scope=1;
       displayName="Race Left Wood Sign 3";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\rezu_race_left.paa"
       };
    };
    class RaceRightWoodSign3: EvocatusWoodSign3Base
    {
       scope=1;
       displayName="Race Right Wood Sign 3";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\rezu_race_right.paa"
       };
    };

    class RaceFinishBillboard3: EvocatusBillboard3Base
    {
        scope=1;
        displayName="Race Finish Billboard 3";
        hiddenSelectionsTextures[]=
        {
            "Rezurrection_Server\data\Custom\rezu_race_finish.paa"
        };
    };
    class RaceLeftBillboard3: EvocatusBillboard3Base
    {
       scope=1;
       displayName="Race Left Billboard 2";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\rezu_race_left.paa"
       };
    };
    class RaceRightBillboard3: EvocatusBillboard3Base
    {
       scope=1;
       displayName="Race Right Billboard 3";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\rezu_race_right.paa"
       };
    };

    class RaceFinishFatBillboard3: EvocatusFatBillboard3Base
    {
       scope=1;
       displayName="Race Finish Fat Billboard 3";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\rezu_race_finish.paa"
       };
    };
    class RaceLeftFatBillboard3: EvocatusFatBillboard3Base
    {
       scope=1;
       displayName="Race Left Fat Billboard 3";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\rezu_race_left.paa"
       };
    };
    class RaceRightFatBillboard3: EvocatusFatBillboard3Base
    {
       scope=1;
       displayName="Race Right Fat Billboard 3";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\rezu_race_right.paa"
       };
    };

    class RaceFinishFatBillboard2: EvocatusFatBillboard2Base
    {
       scope=1;
       displayName="Race Finish Fat Billboard 2";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\rezu_race_finish.paa",
           "Rezurrection_Server\data\Custom\rezu_race_finish.paa"
       };
    };
    class RaceLeftFatBillboard2: EvocatusFatBillboard2Base
    {
       scope=1;
       displayName="Race Left Fat Billboard 2";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\rezu_race_left.paa",
           "Rezurrection_Server\data\Custom\rezu_race_left.paa"
       };
    };
    class RaceRightFatBillboard2: EvocatusFatBillboard2Base
    {
       scope=1;
       displayName="Race Right Fat Billboard 2";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\rezu_race_right.paa",
           "Rezurrection_Server\data\Custom\rezu_race_right.paa"
       };
    };

    class RaceFinishFatBillboard1: EvocatusFatBillboard1Base
    {
       scope=1;
       displayName="Race Finish Fat Billboard 1";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\rezu_race_finish.paa"
       };
    };
    class RaceLeftFatBillboard1: EvocatusFatBillboard1Base
    {
       scope=1;
       displayName="Race Left Fat Billboard 1";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\rezu_race_left.paa"
       };
    };
    class RaceRightFatBillboard1: EvocatusFatBillboard1Base
    {
       scope=1;
       displayName="Race Right Fat Billboard 1";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\rezu_race_right.paa"
       };
    };

    class WoodSign1258: EvocatusWoodSign3Base
    {
       scope=1;
       displayName="1258";
       hiddenSelectionsTextures[]=
       {
           "Rezurrection_Server\data\Custom\1258.paa"
       };
    };

}

class CfgMods
{
	class Rezurrection_Server_1
	{
	    dir = "Rezurrection_Server";
	    picture = "";
	    action = "";
	    hideName = 1;
	    hidePicture = 1;
	    name = "Plus200";
	    credits = "Plus200";
	    author = "Plus200";
	    authorID = "76561198078441547";
	    version = "0.1";
	    extra = 0;
	    type = "mod";

	    dependencies[] = {"World", "Game"};

	    class defs
	    {
			class worldScriptModule
            {
                value = "";
                files[] = {"Rezurrection_Server/scripts/4_World"};
            };
			class gameScriptModule {
				value = "";
				files[] = {"Rezurrection_Server/scripts/3_Game"};
			};
        };
    };
};
