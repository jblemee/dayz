modded class PluginRecipesManager
{
	override void RegisterRecipies()
	{
        super.RegisterRecipies();
	    Print("[PLUS200 RECIPES] Register recipes");

		RegisterRecipe(new P200CraftRagFromPlantMaterials);
		RegisterRecipe(new P200CraftSewingKit);
		RegisterRecipe(new SharpenStick);
		RegisterRecipe(new P200CraftWhetStoneFromStoneKnife);
		RegisterRecipe(new P200CraftNailsFromSmallStone);
		RegisterRecipe(new P200CraftBandageDressing);
		RegisterRecipe(new P200CraftLeatherSewingKit);
		RegisterRecipe(new P200CraftNettingFromRags);

		RegisterRecipe(new P200CraftNailsBox);
		RegisterRecipe(new P200DecraftJacketCoat);
		RegisterRecipe(new P200DecraftStone);
		RegisterRecipe(new P200DecraftAllClothing);
		RegisterRecipe(new P200DecraftHescobox);

		// Bags
        RegisterRecipe(new P200DecraftBags);
        RegisterRecipe(new P200CraftUtilityButtPack);
        RegisterRecipe(new P200CraftAssaultBackpack);
        RegisterRecipe(new P200CraftHuntingBag);
        RegisterRecipe(new P200CraftCoyoteBag);
        RegisterRecipe(new P200CraftTortillaBag);
        RegisterRecipe(new P200CraftAliceBagGreen);
        RegisterRecipe(new P200CraftAliceBagCamo);
        RegisterRecipe(new P200Craft_BurlapSackFromStrip);
        RegisterRecipe(new P200Craft_BurlapSackFromRag);

        //Barrels
        RegisterRecipe(new P200CraftBarrelBlue);
        RegisterRecipe(new P200CraftBarrelRed);
        RegisterRecipe(new P200CraftBarrelGreen);
        RegisterRecipe(new P200CraftBarrelYellow);

        //Tents
        RegisterRecipe(new P200CraftCarTent);
        RegisterRecipe(new P200CraftMilitaryTent);
        RegisterRecipe(new P200CraftMediumTent);
        RegisterRecipe(new P200DecraftTent);


        //Tools
        RegisterRecipe(new P200CraftToolHandle);
        RegisterRecipe(new P200CraftImprovisedBaseballBat);
        RegisterRecipe(new P200CraftImprovisedHacksaw);
        RegisterRecipe(new P200CraftImprovisedHandsaw);
        RegisterRecipe(new P200CraftImprovisedShovel);
        RegisterRecipe(new P200CraftImprovisedAxe);
        RegisterRecipe(new P200CraftImprovisedPickaxe);
        RegisterRecipe(new P200CraftImprovisedHammer);
        RegisterRecipe(new P200CraftImprovisedHatchet);
        RegisterRecipe(new P200CraftImprovisedSledgeHammer);
        RegisterRecipe(new P200CraftPliers);
        RegisterRecipe(new P200CraftCrowbar);
        RegisterRecipe(new P200CraftBlowTorch);

        // Misc
        RegisterRecipe(new P200CraftCamonet);
        RegisterRecipe(new P200CraftNail);
        RegisterRecipe(new P200CraftTannedLeatherFromPelt);
        RegisterRecipe(new P200CraftGardenLime);
        RegisterRecipe(new P200FillGardenLime);
        RegisterRecipe(new P200CraftDuctTapeFromRag);
        RegisterRecipe(new P200CraftDuctTapeFromStrip);

        // Craft furniture
        //        RegisterRecipe(new CraftFridge);

        // Cars
        RegisterRecipe(new P200CraftTruckBattery);
        RegisterRecipe(new P200CutCarDoor);
	}
}