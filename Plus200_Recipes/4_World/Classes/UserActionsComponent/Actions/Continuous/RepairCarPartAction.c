class P200RepairCarPartActionCB : ActionContinuousBaseCB
{
	override void CreateActionComponent()
	{
		m_ActionData.m_ActionComponent = new CAContinuousTime(UATimeSpent.RESTRAIN);
	}
};

class P200ActionRepairCarPart : ActionContinuousBase
{
	void P200ActionRepairCarPart()
	{
		m_CallbackClass = P200RepairCarPartActionCB;
		m_CommandUID = DayZPlayerConstants.CMD_ACTIONFB_CRAFTING;
        m_StanceMask = DayZPlayerConstants.STANCEMASK_ERECT | DayZPlayerConstants.STANCEMASK_CROUCH;
		m_SpecialtyWeight = UASoftSkillsWeight.ROUGH_LOW;

		m_FullBody = true;
		m_LockTargetOnUse = true;
	}

	override void CreateConditionComponents()
	{
        m_ConditionTarget = new CCTNone;
        m_ConditionItem = new CCINonRuined;
	}

	override string GetText()
	{
		return "#repair";
	}

	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		if ( GetGame().IsMultiplayer() && GetGame().IsServer() )
				return true;

		Object targetObject = target.GetObject();

		if ( targetObject.IsInherited(CarDoor) && item.IsInherited(EpoxyPutty))
		{
			CarDoor door = CarDoor.Cast(targetObject);
			if(door.GetType().IndexOf("Rust") >= 0 || (door.GetHealthLevel() > GameConstants.STATE_PRISTINE && door.GetHealthLevel() < GameConstants.STATE_RUINED ))
			{
				return item.GetQuantity() > 90;	
			}

		}

		return false;
	}

	override void OnFinishProgressServer(ActionData action_data)
	{
	        CarDoor door = CarDoor.Cast(action_data.m_Target.GetObject());
            ItemBase m_EpoxyPutty = ItemBase.Cast(action_data.m_MainItem);
            PlayerBase m_Player = PlayerBase.Cast(action_data.m_Player);
			CarDoor newDoor;
            string doorType = door.GetType();
            float pos_y = GetGame().SurfaceY(door.GetPosition()[0], door.GetPosition()[2]);
            vector newPos = Vector(door.GetPosition()[0], pos_y + door.GetPosition()[1] , door.GetPosition()[2]);
            vector newOrientation = door.GetOrientation();

			doorType.Replace("Rust", "");
			Object newDoorObj = GetGame().CreateObject(doorType, newPos);
			if(newDoorObj)  {
                door.Delete();
                newDoor = CarDoor.Cast(newDoorObj);
                newDoor.SetOrientation(newOrientation);
			}

            m_EpoxyPutty.AddQuantity(-90, false);
            action_data.m_Player.GetSoftSkillsManager().AddSpecialty(m_SpecialtyWeight);
	}
};