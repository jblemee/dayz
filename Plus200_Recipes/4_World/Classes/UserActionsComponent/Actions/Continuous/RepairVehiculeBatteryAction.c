class P200RepairVehiculeBatteryActionCB : ActionContinuousBaseCB
{
	override void CreateActionComponent()
	{
		m_ActionData.m_ActionComponent = new CAContinuousTime(UATimeSpent.RESTRAIN);
	}
};

class P200ActionRepairVehiculeBattery : ActionContinuousBase
{
	void P200ActionRepairVehiculeBattery()
	{
		m_CallbackClass = P200RepairVehiculeBatteryActionCB;
		m_CommandUID = DayZPlayerConstants.CMD_ACTIONFB_CRAFTING;
        m_StanceMask = DayZPlayerConstants.STANCEMASK_ERECT | DayZPlayerConstants.STANCEMASK_CROUCH;
		m_SpecialtyWeight = UASoftSkillsWeight.ROUGH_LOW;

		m_FullBody = true;
		m_LockTargetOnUse = true;
	}

	override void CreateConditionComponents()
	{
        m_ConditionTarget = new CCTNone;
        m_ConditionItem = new CCINonRuined;
	}

	override string GetText()
	{
		return "#repair";
	}

	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		if ( GetGame().IsMultiplayer() && GetGame().IsServer() )
				return true;

		Object targetObject = target.GetObject();

		if ( targetObject.IsInherited(VehicleBattery) && item.IsInherited(ElectronicRepairKit))
		{
			VehicleBattery battery = VehicleBattery.Cast(targetObject);
			if(battery.GetHealthLevel() > GameConstants.STATE_PRISTINE && battery.GetHealthLevel() < GameConstants.STATE_RUINED )
			{
				return item.GetQuantity() > 20;
			}

		}

		return false;
	}

	override void OnFinishProgressServer(ActionData action_data)
	{
	        VehicleBattery battery = VehicleBattery.Cast(action_data.m_Target.GetObject());
            ItemBase m_electric_kit = ItemBase.Cast(action_data.m_MainItem);
            PlayerBase m_Player = PlayerBase.Cast(action_data.m_Player);
			VehicleBattery newBattery;
            string batteryType = battery.GetType();
            float pos_y = GetGame().SurfaceY(battery.GetPosition()[0], battery.GetPosition()[2]);
            vector newPos = Vector(battery.GetPosition()[0], pos_y + battery.GetPosition()[1] , battery.GetPosition()[2]);
            vector newOrientation = battery.GetOrientation();

			Object newBatteryObj = GetGame().CreateObject(batteryType, newPos);
			if(newBatteryObj)  {
                battery.Delete();
                newBattery = VehicleBattery.Cast(newBatteryObj);
                newBattery.SetOrientation(newOrientation);
			}

            m_electric_kit.AddQuantity(-20, false);
            action_data.m_Player.GetSoftSkillsManager().AddSpecialty(m_SpecialtyWeight);
	}
};