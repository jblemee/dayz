class CfgPatches
{
  class P200_Surfaces
  {
    units[] = {};
    weapons[] = {};
    requiredVersion = 0.1;
    requiredAddons[] = {"DZ_Surfaces"};
    author = "plus200";
    name = "Plus200_Surfaces";
    url = "https://gitlab.com/jblemee/dayz";
  };
};

class CfgSurfaces
{
    class DZ_SurfacesInt; // for interior surface
    class DZ_SurfacesExt; // for exterior surface

    class P200_beach_sand: DZ_SurfacesExt
    {
        files = "beach_sand*";

        friction = 0.98;
        restitution = 0.55;
        vpSurface = "Gravel";

        soundEnviron = "dirt";
        soundHit = "soft_ground";

        character = "cp_concrete_grass";

        footDamage = 0.0;
        audibility = 1.0;
        isDigable = 0;
        isFertile = 0;

        impact = "Hit_Sand";
        deflection = 0.1;
    };

    class P200_beach_wet_sand: DZ_SurfacesExt
    {
        files = "beach_wet_sand*";

        friction = 0.98;
        restitution = 0.55;
        vpSurface = "Gravel";

        soundEnviron = "dirt";
        soundHit = "soft_ground";

        character = "cp_concrete_grass";

        footDamage = 0.0;
        audibility = 1.0;
        isDigable = 0;
        isFertile = 0;

        impact = "Hit_Sand";
        deflection = 0.1;
    };

    class CfgSoundTables
    {
        class CfgStepSoundTables
        {
            #include "sounds_animals.hpp"
            #include "sounds_infected.hpp"
            #include "sounds_character.hpp"
        };
    };
};
