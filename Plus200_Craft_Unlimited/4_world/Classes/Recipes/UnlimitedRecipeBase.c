class UnlimitedRecipeBase : RecipeBase
{
    ConfigRecipe recipes;

    void UnlimitedRecipeBase(ConfigRecipe recipes) {
        this.recipes = recipes;
        this.Init();
    }

    override void Init()
	{
	    if(!this.recipes) return;
	    m_Name = this.recipes.m_Name;
        m_IsInstaRecipe = this.recipes.m_IsInstaRecipe;
        m_AnimationLength = this.recipes.m_AnimationLength;
        m_Specialty = this.recipes.m_Specialty;

        for(int i=0; i < this.recipes.ingredients.Count(); i++) {
            ConfigIngredient ingredient = this.recipes.ingredients[i];
            m_MinDamageIngredient[i] = ingredient.m_MinDamageIngredient;
            m_MaxDamageIngredient[i] = ingredient.m_MaxDamageIngredient;

            m_MinQuantityIngredient[i] = ingredient.m_MinQuantityIngredient;
            m_MaxQuantityIngredient[i] = ingredient.m_MaxQuantityIngredient;

            for(int j=0; j< ingredient.items.Count(); j++) {
                InsertIngredient(i, ingredient.items[j]);
            }
            m_IngredientAddHealth[i] = ingredient.m_IngredientAddHealth;
            m_IngredientSetHealth[i] = ingredient.m_IngredientSetHealth;
            m_IngredientAddQuantity[i] = ingredient.m_IngredientAddQuantity;
            m_IngredientDestroy[i] = ingredient.m_IngredientDestroy;
            m_IngredientUseSoftSkills[i] = ingredient.m_IngredientUseSoftSkills;
        }

        for(int k=0; k< this.recipes.results.Count(); k++) {
            ConfigResult result = this.recipes.results[k];
            AddResult( result.m_item);
            m_ResultSetFullQuantity[k] = result.m_ResultSetFullQuantity;
            m_ResultSetQuantity[k] =  result.m_ResultSetQuantity;
            m_ResultSetHealth[k] =  result.m_ResultSetHealth;
            m_ResultInheritsHealth[k] =  result.m_ResultInheritsHealth;
            m_ResultInheritsColor[k] =  result.m_ResultInheritsColor;
            m_ResultToInventory[k] =  result.m_ResultToInventory;
            m_ResultUseSoftSkills[k] =  result.m_ResultUseSoftSkills;
            m_ResultReplacesIngredient[k] =  result.m_ResultReplacesIngredient;
        }
	}

	override bool CanDo(ItemBase ingredients[], PlayerBase player)
	{
          return true; // ingredients[0].IsEmpty() && ingredients[1].IsEmpty();
	}
}