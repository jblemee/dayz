modded class CraftImprovisedSledgeHammer
{
	override bool CanDo(ItemBase ingredients[], PlayerBase player)//final check for recipe's validity
	{
		return false;
	}
}

modded class CraftImprovisedHacksaw
{
	override bool CanDo(ItemBase ingredients[], PlayerBase player)//final check for recipe's validity
	{
		return false;
	}
}

modded class PluginRecipesManager
{
	override void RegisterRecipies()
	{
		super.RegisterRecipies();

		RegisterRecipe(new RezuCraftWaterPouch);
	}
}