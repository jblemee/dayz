class P200ItemProbability {
	string item;
	int probability;

	void P200ItemProbability(string item, int prob) {
	    this.item = item;
	    this.probability = prob;
	}
}

class P200MinableItemConfig
{
    string actionText;
    int max_mine = -1;
    ref array<string> enabledTypes;
    ref array<ref P200ItemProbability> itemsProbabilities;

    void P200MinableItemConfig( ) {
        this.enabledTypes = new ref array<string>;
        this.itemsProbabilities = new ref array< ref P200ItemProbability>;
    }

    ref array<ref P200ItemProbability> getProbabilities() {
        return this.itemsProbabilities;
    }

    bool hasType(string type) {
        return this.enabledTypes.Find(type) > -1;
    }
}

class P200MiningConfig extends PluginBase
{
	ref array<ref P200MinableItemConfig> buildings = new array<ref P200MinableItemConfig>;
    ref map<string, int> minedItemsProbabilities = new map<string, int>;
    const static ref map<string, int> mineCount = new map<string, int>;
	private const static string FileFolder = "$profile:Plus200\\Mining\\";
	private const static string FilePath = FileFolder + "config.json";

    override void OnInit()
    {
        Print("[Plus200_Mining] Plus200MiningConfig::OnInit()");
        this.LoadConfig();
    }

	void Init()
	{
	    this.CheckFolder();
        this.minedItemsProbabilities.Set("Stone", 200);
        ref P200MinableItemConfig itemExample = new P200MinableItemConfig();
        itemExample.actionText =  "Configurable Action Text";
        itemExample.enabledTypes = {"Land_Train_Wagon_Box", "Land_Boat_Small3"};
        itemExample.itemsProbabilities =  {new ref P200ItemProbability("Pipe", 50)};
        this.buildings.Insert(itemExample );
        JsonFileLoader<P200MiningConfig>.JsonSaveFile(FilePath, this);
	}

    ref map<string, int> getProbabilities() {
    	return this.minedItemsProbabilities;
    }

    ref array<ref P200ItemProbability> getBuildingProbabilities(string buildingType) {
        for(int i = 0; i < this.buildings.Count(); i++)
        {
            P200MinableItemConfig config = this.buildings.Get(i);
            if(config.hasType(buildingType)) {
                return config.getProbabilities();
            }
        }

        return null;
    }

    ref P200MinableItemConfig getItemConfig(string buildingType) {

        for(int i = 0; i < this.buildings.Count(); i++)
        {
            P200MinableItemConfig config = this.buildings.Get(i);
            if(config.hasType(buildingType)) {
                return config;
            }
        }

        return null;
    }

    bool hasType(string itemType) {
        for(int i = 0; i < this.buildings.Count(); i++)
        {
            P200MinableItemConfig config = this.buildings.Get(i);
            if(config.hasType(itemType)) {
               return true;
            }
        }

        return false;
    }

    string GetText(string itemType) {
        for(int i = 0; i < this.buildings.Count(); i++)
        {
            P200MinableItemConfig config = this.buildings.Get(i);
            if(config.hasType(itemType)) {
               return config.actionText;
            }
        }

        return "Default Action";
    }

	void LoadConfig()
	{
		Print("[Plus200_Mining] Started loading config");
		if(!FileExist(FilePath)) {
            this.Init();
		}
        JsonFileLoader<P200MiningConfig>.JsonLoadFile(FilePath, this);

		int buildingCounts = this.buildings.Count();
        for(int i = 0; i < this.buildings.Count(); i++)
        {
           P200MinableItemConfig config = this.buildings.Get(i);
           for(int j = 0; j < config.enabledTypes.Count();j++)
           {
				g_P200MiningClientConfig.Set(config.enabledTypes.Get(j),  config.actionText);
			}
		}
        Print("[Plus200_Mining] Finished loading config");
	}

    void sendToClient()
    {
		int buildingCounts = this.buildings.Count();
        for(int i = 0; i < this.buildings.Count(); i++)
        {
           P200MinableItemConfig config = this.buildings.Get(i);
           for(int j = 0; j < config.enabledTypes.Count();j++)
           {
               GetRPCManager().SendRPC( "P200MiningRPC", "P200MiningRPCPopulateConfig", new Param2< string, string >( config.enabledTypes.Get(j), config.actionText ));
           }
        }

    }

    static void CheckFolder(){
        if (!FileExist(P200MiningConfig.FileFolder))
        {
            if (!FileExist("$profile:\\Plus200\\")) {
                MakeDirectory("$profile:\\Plus200\\");
            }
            MakeDirectory(P200MiningConfig.FileFolder);
        }
    }
};
