class P200RepairCarRadiatorActionCB : ActionContinuousBaseCB
{
	override void CreateActionComponent()
	{
		m_ActionData.m_ActionComponent = new CAContinuousTime(UATimeSpent.RESTRAIN);
	}
};

class P200ActionRepairCarRadiator : ActionContinuousBase
{
	void P200ActionRepairCarRadiator()
	{
		m_CallbackClass = P200RepairCarRadiatorActionCB;
		m_CommandUID = DayZPlayerConstants.CMD_ACTIONFB_CRAFTING;
        m_StanceMask = DayZPlayerConstants.STANCEMASK_ERECT | DayZPlayerConstants.STANCEMASK_CROUCH;
		m_SpecialtyWeight = UASoftSkillsWeight.ROUGH_LOW;

		m_FullBody = true;
		m_LockTargetOnUse = true;
	}

	override void CreateConditionComponents()
	{
        m_ConditionTarget = new CCTNone;
        m_ConditionItem = new CCINonRuined;
	}

	override string GetText()
	{
		return "#repair";
	}

	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		if ( GetGame().IsMultiplayer() && GetGame().IsServer() )
				return true;

		Object targetObject = target.GetObject();

		if ( targetObject.IsInherited(CarRadiator) && item.IsInherited(EpoxyPutty))
		{
			CarRadiator radiator = CarRadiator.Cast(targetObject);
			if(radiator.GetHealthLevel() > GameConstants.STATE_PRISTINE && radiator.GetHealthLevel() < GameConstants.STATE_RUINED )
			{
				return item.GetQuantity() > 90;	
			}

		}

		return false;
	}

	override void OnFinishProgressServer(ActionData action_data)
	{
	        CarRadiator radiator = CarRadiator.Cast(action_data.m_Target.GetObject());
            ItemBase m_EpoxyPutty = ItemBase.Cast(action_data.m_MainItem);
            PlayerBase m_Player = PlayerBase.Cast(action_data.m_Player);
			CarRadiator newRadiator;
            float pos_y = GetGame().SurfaceY(radiator.GetPosition()[0], radiator.GetPosition()[2]);
            vector newPos = Vector(radiator.GetPosition()[0], pos_y + radiator.GetPosition()[1] , radiator.GetPosition()[2]);
            vector newOrientation = radiator.GetOrientation();

			Object newRadiatorObj = GetGame().CreateObject("CarRadiator", newPos);
			if(newRadiatorObj)  {
                radiator.Delete();
                newRadiator = CarRadiator.Cast(newRadiatorObj);
                newRadiator.SetOrientation(newOrientation);
			}

            m_EpoxyPutty.AddQuantity(-90, false);
            action_data.m_Player.GetSoftSkillsManager().AddSpecialty(m_SpecialtyWeight);
	}
};