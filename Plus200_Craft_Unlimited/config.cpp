class CfgPatches
{
	class Plus200_Craft_Unlimited
	{
		units[]={};
		weapons[]={};
		requiredVersion=0.1;
		requiredAddons[]=
		{
			"DZ_Data"
		};
	};
};
class CfgMods
{
	class Plus200_Craft_Unlimited_1
	{
		dir="Plus200_Craft_Unlimited";
		picture="";
		action="";
		hideName=1;
		hidePicture=1;
		name="Plus200_Mining";
		credits="Plus200";
		author="Plus200";
		authorID="76561198078441547";
		version="0.1";
		extra=0;
		type="mod";
		dependencies[]=
		{
			"World",
			"Mission"
		};
		class defs
		{
			class worldScriptModule
			{
				value="";
				files[]=
				{
					"Plus200_Craft_Unlimited/4_world"
				};
			};
			class missionScriptModule
			{
				value="";
				files[]=
				{
					"Plus200_Craft_Unlimited/5_Mission"
				};
			};
		};
	};
};

