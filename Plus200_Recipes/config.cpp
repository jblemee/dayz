class CfgPatches
{
	class Plus200_Recipes
	{
		units[]={};
		weapons[]={};
		requiredVersion=0.1;
		requiredAddons[]=
		{
			"DZ_Characters",
			"DZ_Characters_Backpacks",
			"DZ_Characters_Pants",
			"DZ_Characters_Tops",
			"DZ_Gear_Containers",
			"DZ_Characters_Headgear",
			"DZ_Gear_Camping",
			"DZ_Gear_Crafting",
			"DZ_Gear_Tools",
			"DZ_Gear_Consumables",
			"DZ_Animals",
            "DZ_Data",
            "DZ_Vehicles_Wheeled",
            "DZ_Weapons_Melee"
		};
	};
};

class CfgSlots
{
	class Slot_DuctTape
	{
		name = "DuctTape";
		displayName = "DuctTape";
		ghostIcon = "default";
	};
	class Slot_EpoxyPutty
	{
		name = "EpoxyPutty";
		displayName = "EpoxyPutty";
		ghostIcon = "default";
	};
	class Slot_ToolHandle
    	{
    		name = "ToolHandle";
    		displayName = "ToolHandle";
    		ghostIcon = "default";
    	};
};

class CfgMods
{
	class Plus200_Recipes_1
	{
		dir="Plus200_Recipes";
		picture="";
		action="";
		hideName=1;
		hidePicture=1;
		name="Plus200";
		credits="Plus200";
		author="Plus200";
		authorID="76561198078441547";
		version="0.1";
		extra=0;
		type="mod";
		dependencies[]=
		{
			"World"
		};
		class defs
		{
			class worldScriptModule
			{
				value="";
				files[]=
				{
					"Plus200_Recipes/4_World"
				};
			};
		};
	};
};

class CfgVehicles
{
    class Inventory_Base;
	class BaseballBat;
	class Container_Base;
	class WoodenCrate;

	//******* FIX Car repairing ***/
	class Sedan_02_Door_1_1;
	class Sedan_02_Door_1_2;
	class Sedan_02_Door_2_1;
	class Sedan_02_Door_2_2;
	class Sedan_02_Hood;
	class Sedan_02_Trunk;

	class Sedan_02_Door_1_1_Yellow : Sedan_02_Door_1_1 {};
    class Sedan_02_Door_1_2_Yellow : Sedan_02_Door_1_2 {};
    class Sedan_02_Door_2_1_Yellow : Sedan_02_Door_2_1 {};
    class Sedan_02_Door_2_2_Yellow : Sedan_02_Door_2_2 {};
    class Sedan_02_Hood_Yellow : Sedan_02_Hood {};
    class Sedan_02_Trunk_Yellow : Sedan_02_Trunk {};

    class HatchbackDoors_Driver;
    class HatchbackDoors_CoDriver;
    class HatchbackHood;
    class HatchbackTrunk;

    class HatchbackDoors_Driver_Green: HatchbackDoors_Driver {};
    class HatchbackDoors_CoDriver_Green: HatchbackDoors_CoDriver {};
    class HatchbackHood_Green: HatchbackHood {};
    class HatchbackTrunk_Green: HatchbackTrunk {};

    class CivSedanDoors_Driver;
    class CivSedanDoors_CoDriver;
    class CivSedanDoors_BackLeft;
    class CivSedanDoors_BackRight;
    class CivSedanHood;
    class CivSedanTrunk;

    class CivSedanDoors_Driver_White : CivSedanDoors_Driver {};
    class CivSedanDoors_CoDriver_White : CivSedanDoors_CoDriver {};
    class CivSedanDoors_BackLeft_White : CivSedanDoors_BackLeft {};
    class CivSedanDoors_BackRight_White : CivSedanDoors_BackRight {};
    class CivSedanHood_White : CivSedanHood {};
    class CivSedanTrunk_White : CivSedanTrunk {};

    //********************************/

    class DuctTape : Inventory_Base
    {
        inventorySlot = "DuctTape";
    };
    class EpoxyPutty : Inventory_Base
    {
        inventorySlot = "EpoxyPutty";
    };
	class P200ToolHandle: BaseballBat
	{
		scope=2;
		displayName="#str_tool_handle_display_name";
		descriptionShort="#str_tool_handle_description";
        inventorySlot = "ToolHandle";
	};

    class Stone;
    class P200IronOre: Stone
    {
        scope=2;
        displayName="#str_iron_ore_display_name";
        descriptionShort="#str_iron_ore_description";
        model="\dz\gear\consumables\Stone.p3d";
        inventorySlot="Material_IronOre";
        weight=1250;
        hiddenSelections[]=
        {
            "zbytek",
            "component01"
        };
        hiddenSelectionsTextures[]=
        {
            "\Plus200_Recipes\Data\IronOre.paa",
            "\Plus200_Recipes\Data\IronOre.paa"
        };
    };

    class P200WorkBox: WoodenCrate
    {
        displayName="#str_tool_box_display_name";
        descriptionShort="#str_tool_box_description";
        attachments[] =
        {
            "DuctTape",
            "Material_Nails",
            "Material_WoodenPlanks",
            "Material_MetalWire",
            "EpoxyPutty",
            "ToolHandle"
        };
    };

    class P200DrillPress: Inventory_Base
    {
        scope=2;
        displayName="Drill Press";
        descriptionShort="Perfect tool for drilling perfect holes, or machining weapons parts or ammunitions...";
        model="\DZ\structures\furniture\various\Drill.p3d";
        bounding="BSphere";
        overrideDrawArea="3.0";
        forceFarBubble="true";
        handheld="true";
        carveNavmesh=1;
        canBeDigged=0;
        weight=20000;
        itemSize[]={10,10};
        physLayer="item_large";
        rotationFlags=0;
        class Cargo
        {
            itemsCargoSize[]={2,1};
            openable=0;
            allowOwnedCargoManipulation=1;
        };
    };

    class P200PC: Inventory_Base
    	{
    		scope=2;
    		displayName="Old Computer";
    		descriptionShort="Broken at the moment, maybe one day we could fix it and use it ....";
    		model="\DZ\structures\furniture\eletrical_appliances\pc\PC.p3d";
    		bounding="BSphere";
    		overrideDrawArea="3.0";
    		forceFarBubble="true";
    		handheld="true";
    		carveNavmesh=1;
    		canBeDigged=0;
    		weight=15000;
    		itemSize[]={10,10};
    		physLayer="item_large";
    		rotationFlags=0;
    		class Cargo
    		{
    			itemsCargoSize[]={4,4};
    			openable=0;
    			allowOwnedCargoManipulation=1;
    		};
    	};
    class P200GarbageBin: Inventory_Base
    {
        scope=2;
        displayName="Garbage Bin";
        descriptionShort="Put your trash in this bin";
        model="\DZ\structures\residential\misc\garbage_bin.p3d";
        bounding="BSphere";
        overrideDrawArea="3.0";
        forceFarBubble="true";
        handheld="true";
        carveNavmesh=1;
        canBeDigged=0;
        weight=25000;
        itemSize[]={10,10};
        physLayer="item_large";
        rotationFlags=0;
        class Cargo
        {
            itemsCargoSize[]={6,6};
            openable=0;
            allowOwnedCargoManipulation=1;
        };
    };
    class P200ShelfSmall: Inventory_Base
    {
        scope=2;
        displayName="Small Shelf";
        descriptionShort="...";
        model="\DZ\structures\furniture\various\shelf_dz.p3d";
        bounding="BSphere";
        overrideDrawArea="3.0";
        forceFarBubble="true";
        handheld="true";
        carveNavmesh=1;
        canBeDigged=0;
        weight=50000;
        itemSize[]={10,10};
        physLayer="item_large";
        rotationFlags=0;
        attachments[]=
        {
            "WoodenCase",
            "300RoundBox",
            "ProtectorCase",
            "firstaidkit",
            "waterproofbag"
        };
        class GUIInventoryAttachmentsProps
        {
            class Shelf_Row_1
            {
                name="Row 1";
                description="";
                attachmentSlots[]=
                {
                    "WoodenCase"
                };
                icon="missing";
            };
            class Shelf_Row_2
            {
                name="Row 2";
                description="";
                attachmentSlots[]=
                {
                    "ProtectorCase",
                    "300RoundBox"
                };
                icon="missing";
            };
            class Shelf_Row_3
            {
                name="Row 3";
                description="";
                attachmentSlots[]=
                {
                    "firstaidkit",
                    "waterproofbag"
                };
                icon="missing";
            };
        };
        class Cargo
        {
            itemsCargoSize[]={6,8};
            openable=0;
            allowOwnedCargoManipulation=1;
        };
        class DamageSystem
        {
            class GlobalHealth
            {
                class Health
                {
                    hitpoints=100;
                };
            };
            class GlobalArmor
            {
                class Projectile
                {
                    class Health
                    {
                        damage=0;
                    };
                    class Blood
                    {
                        damage=0;
                    };
                    class Shock
                    {
                        damage=0;
                    };
                };
            };
        };
    };
    class P200Shelf: Inventory_Base
    {
        scope=2;
        displayName="Large Shelf";
        descriptionShort="...";
        model="P:\DZ\structures\furniture\generalstore\shelf.p3d";
        bounding="BSphere";
        overrideDrawArea="3.0";
        forceFarBubble="true";
        handheld="true";
        carveNavmesh=1;
        canBeDigged=0;
        weight=50000;
        itemSize[]={10,10};
        physLayer="item_large";
        rotationFlags=0;
        attachments[]=
        {
            "Feet",
            "Back",
            "Body",
            "Gloves",
            "Headgear",
            "Eyewear",
            "Legs",
            "WoodenCase"
        };
        class GUIInventoryAttachmentsProps
        {
            class Shelf_Row_1
            {
                name="Row 1";
                description="";
                attachmentSlots[]=
                {
                    "Feet",
                    "Back"
                };
                icon="missing";
            };
            class Shelf_Row_2
            {
                name="Row 2";
                description="";
                attachmentSlots[]=
                {
                    "Body"
                };
                icon="missing";
            };
            class Shelf_Row_3
            {
                name="Row 3";
                description="";
                attachmentSlots[]=
                {
                    "Gloves",
                    "Headgear",
                    "Eyewear"
                };
                icon="missing";
            };
            class Shelf_Row_4
            {
                name="Row 4";
                description="";
                attachmentSlots[]=
                {
                    "Legs"
                };
                icon="missing";
            };
            class Shelf_Row_5
            {
                name="Row 5";
                description="";
                attachmentSlots[]=
                {
                    "WoodenCase"
                };
                icon="missing";
            };
        };
        class Cargo
        {
            itemsCargoSize[]={6,8};
            openable=0;
            allowOwnedCargoManipulation=1;
        };
        class DamageSystem
        {
            class GlobalHealth
            {
                class Health
                {
                    hitpoints=100;
                };
            };
            class GlobalArmor
            {
                class Projectile
                {
                    class Health
                    {
                        damage=0;
                    };
                    class Blood
                    {
                        damage=0;
                    };
                    class Shock
                    {
                        damage=0;
                    };
                };
            };
        };
    };
    class P200Lekarnicka: Inventory_Base
    {
        scope=2;
        displayName="Medical Closet";
        descriptionShort="...";
        model="\DZ\structures\furniture\decoration\lekarnicka\lekarnicka.p3d";
        bounding="BSphere";
        overrideDrawArea="3.0";
        forceFarBubble="true";
        handheld="true";
        carveNavmesh=1;
        canBeDigged=0;
        weight=25000;
        itemSize[]={6,8};
        physLayer="item_large";
        rotationFlags=0;
        attachments[]=
        {
            "firstaidkit"
        };
        class GUIInventoryAttachmentsProps
        {
            class Row_firstaidkit
            {
                name="Row 1";
                description="";
                attachmentSlots[]=
                {
                    "firstaidkit"
                };
                icon="missing";
            };
        };
        class Cargo
        {
            itemsCargoSize[]={6,8};
            openable=0;
            allowOwnedCargoManipulation=1;
        };
    };

    class P200Grinder: Inventory_Base
    {
        scope=2;
        displayName="Metal Grinder";
        descriptionShort="Broken at the moment, maybe one day we could fix it and use it ....";
        model="\DZ\structures\furniture\various\grinder.p3d";
        bounding="BSphere";
        overrideDrawArea="3.0";
        forceFarBubble="true";
        handheld="true";
        carveNavmesh=1;
        canBeDigged=0;
        weight=5000;
        itemSize[]={10,10};
        physLayer="item_large";
        rotationFlags=0;
        class Cargo
        {
        };
        stackedUnit="percentage";
        quantityBar=0;
        varQuantityInit=100;
        varQuantityMin=100;
        varQuantityMax=100;
        repairKitType=4;
    };
    class P200Anvil: Container_Base
    {
        scope=2;
        displayName="Anvil";
        descriptionShort="Broken at the moment, maybe one day we could fix it and use it ....";
        model="\DZ\structures\furniture\various\anvil.p3d";
        bounding="BSphere";
        overrideDrawArea="3.0";
        forceFarBubble="true";
        handheld="true";
        carveNavmesh=1;
        canBeDigged=0;
        weight=5000;
        itemSize[]={10,10};
        physLayer="item_medium";
        rotationFlags=0;
        class Cargo
        {
            itemsCargoSize[]={4,4};
            openable=0;
            allowOwnedCargoManipulation=1;
        };
    };
    class P200Workbench: Container_Base
    {
        scope=2;
        displayName="Workbench";
        descriptionShort="...";
        model="\DZ\structures\furniture\various\workbench.p3d";
        bounding="BSphere";
        overrideDrawArea="3.0";
        forceFarBubble="true";
        handheld="true";
        carveNavmesh=1;
        canBeDigged=0;
        weight=5000;
        itemSize[]={10,10};
        physLayer="item_large";
        rotationFlags=0;
        class Cargo
        {
            itemsCargoSize[]={8,8};
            openable=0;
            allowOwnedCargoManipulation=1;
        };
    };

    class P200WorkbenchSmall: Container_Base
        {
            scope=2;
            displayName="Workbench";
            descriptionShort="...";
            model="\DZ\structures\furniture\various\workbench_dz.p3d";
            bounding="BSphere";
            overrideDrawArea="3.0";
            forceFarBubble="true";
            handheld="true";
            carveNavmesh=1;
            canBeDigged=0;
            weight=5000;
            itemSize[]={10,10};
            physLayer="item_large";
            rotationFlags=0;
            class Cargo
            {
                itemsCargoSize[]={4,4};
                openable=0;
                allowOwnedCargoManipulation=1;
            };
        };
    class P200Table1: Inventory_Base
    {
        scope=2;
        displayName="Wooden Table";
        descriptionShort="a simple wooden table ....";
        model="\DZ\structures\furniture\various\table_dz.p3d";
        bounding="BSphere";
        overrideDrawArea="3.0";
        forceFarBubble="true";
        handheld="true";
        carveNavmesh=1;
        canBeDigged=0;
        weight=5000;
        itemSize[]={10,10};
        physLayer="item_large";
        rotationFlags=0;
        class Cargo
        {
        };
    };
    class P200SofaNew: Inventory_Base
    {
        scope=2;
        displayName="New Leather Sofa";
        descriptionShort="cumfy leather sofa ....";
        model="\DZ\structures\furniture\chairs\sofa_leather\sofa_leather_new.p3d";
        bounding="BSphere";
        overrideDrawArea="3.0";
        forceFarBubble="true";
        handheld="true";
        carveNavmesh=1;
        canBeDigged=0;
        weight=5000;
        itemSize[]={10,10};
        physLayer="item_large";
        rotationFlags=0;
        class Cargo
        {
            itemsCargoSize[]={4,4};
            openable=0;
            allowOwnedCargoManipulation=1;
        };
    };
     class P200SofaOld: Inventory_Base
        {
            scope=2;
            displayName="Old Leather Sofa";
            descriptionShort="cumfy leather sofa ....";
            model="\DZ\structures\furniture\chairs\sofa_leather\sofa_leather_old.p3d";
            bounding="BSphere";
            overrideDrawArea="3.0";
            forceFarBubble="true";
            handheld="true";
            carveNavmesh=1;
            canBeDigged=0;
            weight=5000;
            itemSize[]={10,10};
            physLayer="item_large";
            rotationFlags=0;
            class Cargo
            {
                itemsCargoSize[]={4,4};
                openable=0;
                allowOwnedCargoManipulation=1;
            };
        };
    class P200Couch: Inventory_Base
    {
        scope=2;
        displayName="Fabric Sofa";
        descriptionShort="cumfy fabric sofa ....";
        model="\DZ\structures\furniture\various\couch_dz.p3d";
        bounding="BSphere";
        overrideDrawArea="3.0";
        forceFarBubble="true";
        handheld="true";
        carveNavmesh=1;
        canBeDigged=0;
        weight=5000;
        itemSize[]={10,10};
        physLayer="item_large";
        rotationFlags=0;
        class Cargo
        {
            itemsCargoSize[]={4,4};
            openable=0;
            allowOwnedCargoManipulation=1;
        };
    };
    class P200SofaCorner: Inventory_Base
    {
        scope=2;
        displayName="Corner Sofa";
        descriptionShort="cumfy corner sofa ....";
        model="\DZ\structures\furniture\chairs\sofacorner\sofacorner.p3d";
        bounding="BSphere";
        overrideDrawArea="3.0";
        forceFarBubble="true";
        handheld="true";
        carveNavmesh=1;
        canBeDigged=0;
        weight=5000;
        itemSize[]={10,10};
        physLayer="item_large";
        rotationFlags=0;
        class Cargo
        {
            itemsCargoSize[]={5,5};
            openable=0;
            allowOwnedCargoManipulation=1;
        };
    };
    class P200ChairOffice: Inventory_Base
    {
        scope=2;
        displayName="Office Chair";
        descriptionShort="Office Chair ....";
        model="\DZ\structures\furniture\chairs\ch_office_b\ch_office_b.p3d";
        bounding="BSphere";
        overrideDrawArea="3.0";
        forceFarBubble="true";
        handheld="true";
        carveNavmesh=1;
        canBeDigged=0;
        weight=5000;
        weight=5000;
        itemSize[]={10,10};
        physLayer="item_large";
        rotationFlags=0;
        class Cargo
        {
        };
    };
    class P200WoodenChair: Inventory_Base
    {
        scope=2;
        displayName="Wooden Chair";
        descriptionShort="Wooden Chair ....";
        model="\DZ\structures\furniture\chairs\ch_mod_c\ch_mod_c.p3d";
        bounding="BSphere";
        overrideDrawArea="3.0";
        forceFarBubble="true";
        handheld="true";
        carveNavmesh=1;
        canBeDigged=0;
        weight=5000;
        itemSize[]={10,10};
        physLayer="item_large";
        rotationFlags=0;
        class Cargo
        {
        };
    };
    class P200HospitalBed: Inventory_Base
    {
        scope=2;
        displayName="Hospital Bed";
        descriptionShort="Hospital Bed ....";
        model="\DZ\structures\furniture\hospital_transport_bed\hospital_transport_bed.p3d";
        bounding="BSphere";
        overrideDrawArea="3.0";
        forceFarBubble="true";
        handheld="true";
        carveNavmesh=1;
        canBeDigged=0;
        weight=5000;
        itemSize[]={10,10};
        physLayer="item_large";
        rotationFlags=0;
        class Cargo
        {
        };
    };
    class P200MetalCrate1: Inventory_Base
    {
        scope=2;
        displayName="Metal Crate";
        descriptionShort="Metal Crate ....";
        model="\DZ\structures\furniture\cases\metalcrate\metalcrate.p3d";
        bounding="BSphere";
        overrideDrawArea="3.0";
        forceFarBubble="true";
        handheld="true";
        carveNavmesh=1;
        canBeDigged=0;
        weight=5000;
        itemSize[]={5,5};
        physLayer="item_large";
        rotationFlags=0;
        class Cargo
        {
            itemsCargoSize[]={10,10};
            openable=0;
            allowOwnedCargoManipulation=1;
        };
    };
    class P200MetalCrate2: Inventory_Base
    {
        scope=2;
        displayName="Metal Crate 2";
        descriptionShort="Metal Crate 2 ....";
        model="\DZ\structures\furniture\cases\metalcrate_02\metalcrate_02.p3d";
        bounding="BSphere";
        overrideDrawArea="3.0";
        forceFarBubble="true";
        handheld="true";
        carveNavmesh=1;
        canBeDigged=0;
        weight=5000;
        itemSize[]={5,5};
        physLayer="item_large";
        rotationFlags=0;
        class Cargo
        {
            itemsCargoSize[]={10,10};
            openable=0;
            allowOwnedCargoManipulation=1;
        };
    };
    class P200Chest: Inventory_Base
    {
        scope=2;
        displayName="Big Wooden Chest";
        descriptionShort="Big Wooden Chest ....";
        model="\DZ\structures\furniture\various\chest_dz.p3d";
        bounding="BSphere";
        overrideDrawArea="3.0";
        forceFarBubble="true";
        handheld="true";
        carveNavmesh=1;
        canBeDigged=0;
        weight=5000;
        itemSize[]={10,10};
        physLayer="item_large";
        rotationFlags=0;
        class Cargo
        {
            itemsCargoSize[]={10,40};
            openable=0;
            allowOwnedCargoManipulation=1;
        };
    };

    class P200MetalCuttingSaw: Inventory_Base
    {
        scope=2;
        displayName="Scie circulaire";
        descriptionShort="Permet de couper du metal ....";
        model="\DZ\structures\furniture\various\metal_cutting_saw.p3d";
        bounding="BSphere";
        overrideDrawArea="3.0";
        forceFarBubble="true";
        handheld="true";
        carveNavmesh=1;
        canBeDigged=0;
        weight=40000;
        itemSize[]={10,10};
        physLayer="item_large";
        rotationFlags=0;
        class Cargo
        {
        };
    };

    class P200Fridge: Inventory_Base
    {
        scope=2;
        displayName="Vieux Frigo";
        descriptionShort="Ne fonctionne plus mais isole bien la nourriture.";
        model="\DZ\structures\furniture\kitchen\fridge\fridge.p3d";
        bounding="BSphere";
        overrideDrawArea="3.0";
        forceFarBubble="true";
        handheld="true";
        carveNavmesh=1;
        canBeDigged=0;
        weight=5000;
        itemSize[]={10,10};
        physLayer="item_large";
        rotationFlags=0;
        class Cargo
        {
            itemsCargoSize[]={6,20};
            openable=0;
            allowOwnedCargoManipulation=1;
        };
    };
    class P200Library: Inventory_Base
    {
        scope=2;
        displayName="Bibliothèque";
        descriptionShort="Pour ranger des livres...";
        model="\DZ\structures\furniture\Cases\library_a\library_a.p3d";
        bounding="BSphere";
        overrideDrawArea="3.0";
        forceFarBubble="true";
        handheld="true";
        carveNavmesh=1;
        canBeDigged=0;
        weight=5000;
        itemSize[]={10,10};
        physLayer="item_large";
        rotationFlags=0;
        class Cargo
        {
            itemsCargoSize[]={10,20};
            openable=0;
            allowOwnedCargoManipulation=1;
        };
    };
    class P200Vise: Inventory_Base
    {
        scope=2;
        displayName="Etau de mécanicien";
        descriptionShort="Pour serrer des choses ou des gens...";
        model="\DZ\structures\furniture\various\vise.p3d";
        bounding="BSphere";
        overrideDrawArea="3.0";
        forceFarBubble="true";
        handheld="true";
        carveNavmesh=1;
        canBeDigged=0;
        weight=45000;
        itemSize[]={10,10};
        physLayer="item_large";
        rotationFlags=0;
        class Cargo
        {
        };
    };
    class P200Bucket: Container_Base
    {
        scope=2;
        displayName="Corbeille";
        descriptionShort="Une simple corbeille, peut devenir un pot de fleur...";
        model="\DZ\structures\furniture\Decoration\bucket\bucket.p3d";
        weight=440;
        itemSize[]={3,5};
        itemsCargoSize[]={3,5};
        allowOwnedCargoManipulation=1;
        lootTag[]=
        {
            "Kitchen"
        };
        lootCategory="Tools";
        class AnimationSources
        {
            class handleRotate
            {
                source="user";
                animPeriod=0.0099999998;
                initPhase=1;
            };
        };
        class AnimEvents
        {
            class SoundWeapon
            {
                class pickUpPotLight
                {
                    soundSet="pickUpPotLight_SoundSet";
                    id=796;
                };
                class pickUpPot
                {
                    soundSet="pickUpPot_SoundSet";
                    id=797;
                };
                class drop
                {
                    soundset="pot_drop_SoundSet";
                    id=898;
                };
            };
        };
    };
    class P200Flower1: P200Bucket
    {
        displayName="Pot de fleur 1";
        descriptionShort="Un pot de fleur";
        model="\DZ\structures\furniture\Decoration\Flowers\flower_01.p3d";
        itemSize[]={3,8};
        itemsCargoSize[]={3,3};
    };
    class P200Flower2: P200Bucket
    {
        displayName="Pot de fleur 2";
        descriptionShort="Un pot de fleur";
        model="\DZ\structures\furniture\Decoration\Flowers\flower_02.p3d";
        itemSize[]={3,8};
        itemsCargoSize[]={3,3};
    };

    class P200BiggerBag: Container_Base
    {
        scope=2;
        displayName="Gros Sac";
        descriptionShort="Un gros sac de rangement bien pratique, mais trop lourd à déplacer.";
        model="\DZ\structures\furniture\various\bag_dz.p3d";
        bounding="BSphere";
        overrideDrawArea="3.0";
        forceFarBubble="true";
        handheld="true";
        carveNavmesh=1;
        canBeDigged=0;
        weight=5000;
        itemSize[]={10,10};
        physLayer="item_large";
        rotationFlags=0;
        class Cargo
        {
            itemsCargoSize[]={8,16};
            openable=0;
            allowOwnedCargoManipulation=1;
        };
    };
};

