modded class ItemBase {
    bool IsDecraftable() {
       return IsEmpty();
    }
}

modded class HelmetBase {
    override bool IsDecraftable() {
       return false;
    }
}

modded class AviatorGlasses {
    override bool IsDecraftable() {
       return false;
    }
}

modded class DesignerGlasses {
    override bool IsDecraftable() {
       return false;
    }
}

modded class ThickFramesGlasses {
    override bool IsDecraftable() {
       return false;
    }
}

modded class ThinFramesGlasses {
    override bool IsDecraftable() {
       return false;
    }
}

modded class SportGlasses_ColorBase {
    override bool IsDecraftable() {
       return false;
    }
}

modded class EpoxyPutty {
	override void SetActions()
	{
		super.SetActions();
		AddAction(ActionFillEpoxy);
        AddAction(P200ActionRepairCarPart);
        AddAction(P200ActionRepairCarRadiator);
        AddAction(P200ActionRepairVehiculeBattery);
	}
}
