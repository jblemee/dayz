modded class MissionGameplay
{
	void MissionGameplay()
    {
		GetRPCManager().AddRPC( "P200MiningRPC", "P200MiningRPCPopulateConfig", this, true );
	}

	override void OnMissionStart()
	{
		super.OnMissionStart();
		Print(g_P200MiningClientConfig);
	}

	void P200MiningRPCPopulateConfig( CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
	{
		Param2< string, string > data;
		if ( !ctx.Read( data ) ) return;

		if( type == CallType.Server ) {}
		else
		{
			g_P200MiningClientConfig.Set(data.param1, data.param2);
		}
	}
}
