class P200DropProbability {
	string item;
	int probability;

	void P200ItemProbability(string item, int prob) {
	    this.item = item;
	    this.probability = prob;
	}
}

class P200ActionConfig
{
    string actionText;
    ref array<string> types;
    ref array<string> tools;
    ref array<ref P200ItemProbability> drops;
    int toolDamages = 10;
    int targetDamages = 0;

    void P200MinableItemConfig( ) {
        this.types = new ref array<string>;
        this.drops = new ref array< ref P200DropProbability>;
    }

    ref array<ref P200DropProbability> getProbabilities() {
        return this.drops;
    }

    bool hasType(string type) {
        return this.types.Find(type) > -1;
    }
}

class P200ActionsUnlimitedConfig extends PluginBase
{
	ref array<ref P200ActionConfig> actions = new array<ref P200ActionConfig>;
	private const static string FileFolder = "$profile:Plus200\\ActionsUnlimited\\";
	private const static string FilePath = FileFolder + "actions.json";

    override void OnInit()
    {
        Print("[Plus200_Action_Unlimited] P200ActionsUnlimitedConfig::OnInit()");
        this.LoadConfig();
    }

	void Init()
	{
	    this.CheckFolder();
        ref P200ActionConfig itemExample = new P200ActionConfig();
        itemExample.actionText =  "Configurable Action Text";
        itemExample.types = {"Land_Train_Wagon_Box", "Land_Boat_Small3"};
        itemExample.drops =  {new ref P200DropProbability("Pipe", 50)};
        this.actions.Insert(itemExample );
        JsonFileLoader<P200ActionsUnlimitedConfig>.JsonSaveFile(FilePath, this);
	}

	ref array<ref P200DropProbability> getDropProbabilities(EntityAI target, EntityAI tool) {
        array<ref P200ActionConfig> actions = getActions(target, tool);
        if(action.Size() > 0 ){
            return action[0].getProbabilities();
        } else {
            return null;
        }
    }

    ref array<ref P200ActionConfig> getActions(EntityAI target, EntityAI tool) {
        return g_getP200UnlimitedActions(target, tool, this.actions);
    }

    bool hasType(string targetType, string toolType) {
        for(int i = 0; i < this.actions.Count(); i++)
        {
            P200ActionConfig config = this.actions.Get(i);
            if(config.hasType(targetType, toolType)) {
               return true;
            }
        }

        return false;
    }

    string GetText(string itemType) {
        for(int i = 0; i < this.actions.Count(); i++)
        {
            P200MinableItemConfig config = this.actions.Get(i);
            if(config.hasType(itemType)) {
               return config.actionText;
            }
        }

        return "Default Action";
    }

	void LoadConfig()
	{
		Print("[P200_Actions_Unlimited] Started loading config");
		if(!FileExist(FilePath)) {
            this.Init();
		}
        JsonFileLoader<P200MiningConfig>.JsonLoadFile(FilePath, this);

		int buildingCounts = this.actions.Count();
        for(int i = 0; i < this.actions.Count(); i++)
        {
           P200MinableItemConfig config = this.actions.Get(i);
           for(int j = 0; j < config.enabledTypes.Count();j++)
           {
				g_P200MiningClientConfig.Set(config.enabledTypes.Get(j),  config.actionText);
			}
		}
        Print("[P200_Actions_Unlimited] Finished loading config");
	}

    void sendToClient()
    {
		int buildingCounts = this.actions.Count();
        for(int i = 0; i < this.actions.Count(); i++)
        {
           P200MinableItemConfig config = this.actions.Get(i);
           for(int j = 0; j < config.enabledTypes.Count();j++)
           {
               GetRPCManager().SendRPC( "P200MiningRPC", "P200MiningRPCPopulateConfig", new Param2< string, string >( config.enabledTypes.Get(j), config.actionText ));
           }
        }

    }

    static void CheckFolder(){
        if (!FileExist(P200MiningConfig.FileFolder))
        {
            if (!FileExist("$profile:\\Plus200\\")) {
                MakeDirectory("$profile:\\Plus200\\");
            }
            MakeDirectory(P200MiningConfig.FileFolder);
        }
    }
};
