class ActionUnlimitedCB : ActionContinuousBaseCB
{
	private const float TIME_BETWEEN_MATERIAL_DROPS = 8;

	override void CreateActionComponent()
	{
		m_ActionData.m_ActionComponent = new CAUnlimited(TIME_BETWEEN_MATERIAL_DROPS);
	}
};

class ActionUnlimited: ActionContinuousBase
{
    P200ActionConfig action;

	void ActionUnlimited()
	{
		m_CallbackClass = ActionUnlimitedCB;
		m_CommandUID = DayZPlayerConstants.CMD_ACTIONFB_MINEROCK;
		m_FullBody = true;
		m_StanceMask = DayZPlayerConstants.STANCEMASK_ERECT;
		m_SpecialtyWeight = UASoftSkillsWeight.ROUGH_HIGH;
	}

	override void CreateConditionComponents()
	{
		m_ConditionTarget = new CCTUnlimited(UAMaxDistances.DEFAULT);
		m_ConditionItem = new CCINonRuined;
	}

	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		EntityAI targetObject = EntityAI.Cast(target.GetObject());
		if (targetObject)
		{
			array<ref P200ActionConfig> actions = getActions(target, tool);
            if(action.Size() > 0 ){
                this.action = action[0];
                return true;
            }
		}
		return false;
	}

	override string GetText()
	{
		return this.action.actionText;
	}

	override string GetActionCommand(m_Player) {
	    return super.GetActionCommand(m_Player);
	}

	override string GetCallbackClassTypename() {
	    return super.GetCallbackClassTypename();
	}

	override ??? GetStanceMask(action_data.m_Player) {
	    return super.GetStanceMask(m_Player);
	}

	override void OnFinishProgressServer( ActionData action_data )
	{
		action_data.m_Player.GetSoftSkillsManager().AddSpecialty( m_SpecialtyWeight );
	}
};