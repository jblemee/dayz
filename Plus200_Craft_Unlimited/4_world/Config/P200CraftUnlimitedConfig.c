class ConfigIngredient
{
    int m_MinDamageIngredient = -1;
    int m_MaxDamageIngredient = 3;

    int m_MinQuantityIngredient = -1;
    int m_MaxQuantityIngredient = -1;

    ref array<string> items = {"Rag"};

    int m_IngredientAddHealth = 0;
    int m_IngredientSetHealth = -1;

    int m_IngredientAddQuantity = 0;

    bool m_IngredientDestroy = true;

    bool m_IngredientUseSoftSkills = false;
}

class ConfigResult
{
    string m_item = "Rag";
    bool m_ResultSetFullQuantity = false;
    int m_ResultSetQuantity = 6;
    int m_ResultSetHealth = -1;
    int m_ResultInheritsHealth = -1;
    int m_ResultInheritsColor = -1;
    int m_ResultToInventory = -2;
    bool m_ResultUseSoftSkills = false;
    int m_ResultReplacesIngredient = -1;
}

class ConfigRecipe
{
    string m_Name = "Duplicate Rag (Plus200_Unlimited_Craft Example)";
    bool m_IsInstaRecipe = false;
    int m_AnimationLength = 2;
    float m_Specialty = 0;
    ref array<ref ConfigIngredient> ingredients = {new ConfigIngredient, new ConfigIngredient};
    ref array<ref ConfigResult> results = {new ConfigResult};
}

class P200CraftUnlimitedConfig extends PluginBase
{
	ref array< ref ConfigRecipe> recipes = new array<ref ConfigRecipe>;
	private const static string FileFolder = "$profile:Plus200\\CraftUnlimited\\";
	private const static string FilePath = FileFolder + "recipes.json";
	private const static ref JsonSerializer m_Serializer = new JsonSerializer;

    override void OnInit()
    {
        Print("[P200CraftUnlimitedConfig] P200CraftUnlimitedConfig::OnInit()");
        this.LoadConfig();
    }

	void Init()
	{
	    this.CheckFolder();
        this.recipes.Insert(new ConfigRecipe);
		JsonFileLoader<array<ref ConfigRecipe>>.JsonSaveFile(FilePath, this.recipes);
	}

	void LoadConfig()
	{
		Print("[P200CraftUnlimitedConfig] Started loading config");
		if(!FileExist(FilePath)) {
            this.Init();
		}
        JsonFileLoader<array<ref ConfigRecipe>>.JsonLoadFile(FilePath, this.recipes);
        Print("[P200CraftUnlimitedConfig] Finished loading config");
	}

    void sendToClient()
    {
        for(int i=0; i<this.recipes.Count(); i++) {
            GetRPCManager().SendRPC( "P200CraftUnlimitedRPC", "P200CraftUnlimitedRPCPopulate", new Param1<ref ConfigRecipe>(this.recipes[i]));
        }
        GetRPCManager().SendRPC( "P200CraftUnlimitedRPC", "P200RefreshRecipes", new Param1<bool>(true));

    }

    static void CheckFolder(){
        if (!FileExist(P200CraftUnlimitedConfig.FileFolder))
        {
            if (!FileExist("$profile:\\Plus200\\")) {
                MakeDirectory("$profile:\\Plus200\\");
            }
            MakeDirectory(P200CraftUnlimitedConfig.FileFolder);
        }
    }

    static string JsonMakeData( ConfigRecipe data )
    {
        string string_data;
        m_Serializer.WriteToString( data, false, string_data );
        return string_data;
    }
};
