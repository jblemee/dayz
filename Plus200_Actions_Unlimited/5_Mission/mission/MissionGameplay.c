modded class MissionGameplay
{
	void MissionGameplay()
    {
		GetRPCManager().AddRPC( "P200ActionsUnlimitedRPC", "P200ActionsUnlimitedRPCPopulateConfig", this, true );
	}

	void P200ActionsUnlimitedRPCPopulateConfig( CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
	{
		Param1< P200ActionConfig > data;
		if ( !ctx.Read( data ) ) return;

		if( type == CallType.Server ) {}
		else
		{
			g_P200ActionsUnlimitedClientConfig.Insert(data.param1);
		}
	}
}
