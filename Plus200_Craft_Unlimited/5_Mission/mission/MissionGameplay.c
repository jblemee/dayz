modded class MissionGameplay
{
    static ref JsonSerializer m_Serializer = new JsonSerializer;

	void MissionGameplay()
    {
		GetRPCManager().AddRPC( "P200CraftUnlimitedRPC", "P200CraftUnlimitedRPCPopulate", this, true );
		GetRPCManager().AddRPC( "P200CraftUnlimitedRPC", "P200RefreshRecipes", this, true );
	}

	override void OnMissionStart()
	{
		super.OnMissionStart();
	}

	void P200CraftUnlimitedRPCPopulate( CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
	{
		Param1<ref ConfigRecipe> data;
		if ( !ctx.Read( data ) ) return;

		if( type != CallType.Server ) {
            PluginRecipesManager recipesManager;
            Class.CastTo(recipesManager, GetPlugin(PluginRecipesManager));
            UnlimitedRecipeBase r = new UnlimitedRecipeBase(data.param1);
            recipesManager.RegisterRecipeUnlock(r);
        }
	}
	void P200RefreshRecipes( CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
    {
        Param1< bool> data;
        if ( !ctx.Read( data ) ) return;

        if( type != CallType.Server ) {
            PluginRecipesManager recipesManager;
            Class.CastTo(recipesManager, GetPlugin(PluginRecipesManager));
            if(data.param1) {
                recipesManager.Refresh();
            }
        }
    }
}
