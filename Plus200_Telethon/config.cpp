
class CfgPatches
{
	class CCG_Server
	{
		units[]={
		    "TelethonSunscreenBillboard",
		    "TelethonWhiteBillboard"
		};
		weapons[]={};
		requiredVersion=0.1;
        requiredAddons[]=
        {
            "evo_EvocatusSigns"
        };
	};
};

class CfgVehicles
{
	class EvocatusBillboard1Base;
	class TelethonSunscreenBillboard: EvocatusBillboard1Base
	{
		scope=1;
		displayName="Telethon Sunscreen Billboard";
		hiddenSelectionsTextures[]=
		{
			"Plus200_Telethon\data\Custom\telethon_sunscreen.paa"
		};
	};
	class TelethonWhiteBillboard: EvocatusBillboard1Base
    {
        scope=1;
        displayName="Telethon White Billboard";
        hiddenSelectionsTextures[]=
        {
            "Plus200_Telethon\data\Custom\telethon.paa"
        };
    };
};

class CfgMods
{
	class CCG_Server_1
	{
	    dir = "Plus200_Telethon";
	    picture = "";
	    action = "";
	    hideName = 1;
	    hidePicture = 1;
	    name = "Plus200";
	    credits = "Plus200";
	    author = "Plus200";
	    authorID = "76561198078441547";
	    version = "0.1";
	    extra = 0;
	    type = "mod";

	    dependencies[] = {};

	    class defs
	    {

        };
    };
};
