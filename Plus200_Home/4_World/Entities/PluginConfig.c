class Plus200HomeConfig
{
	static const string m_FilePath = "$profile:Plus200/Home/home.json";
	static string m_item_class_name = "";
	ref map<string, string> params;
	
	void Plus200HomeConfig(){
	    this.params = new map<string, string>;
	}
	
	static void Init()
	{
	    ref Plus200HomeConfig inst = new Plus200HomeConfig();
		if(FileExist(m_FilePath))
		{
			JsonFileLoader<Plus200HomeConfig>.JsonLoadFile(m_FilePath, inst);
		}
		else
		{
		    Plus200HomeConfig.CheckFolder();
			inst.params["item_class"] = "YourItemClassName";
			JsonFileLoader<Plus200HomeConfig>.JsonSaveFile(m_FilePath, inst);
		}

		Plus200HomeConfig.m_item_class_name = inst.params["item_class"];
	}

	static string GetItemClassName()
	{
	    if(Plus200HomeConfig.m_item_class_name == string.Empty) {
	        Plus200HomeConfig.Init();
	    }

	    return Plus200HomeConfig.m_item_class_name;
	}

	static void CheckFolder(){
	    if (!FileExist(Plus200PlayerHomeConfig.FileFolder)) 
		{
	        if (!FileExist("$profile:\\Plus200\\")) 
			{
                MakeDirectory("$profile:\\Plus200\\");
            }
            MakeDirectory(Plus200PlayerHomeConfig.FileFolder);
            Plus200HomeConfig.Init();
        }
	}
}