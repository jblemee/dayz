class ActionMineBuildingCB : ActionContinuousBaseCB
{
	private const float TIME_BETWEEN_MATERIAL_DROPS = 8;

	override void CreateActionComponent()
	{
		m_ActionData.m_ActionComponent = new CAContinuousMineStatic(TIME_BETWEEN_MATERIAL_DROPS);
	}
};

class ActionMineBuilding: ActionContinuousBase
{
    string actionText;
    P200MiningConfig miningConfig;

	void ActionMineBuilding()
	{
		m_CallbackClass = ActionMineBuildingCB;
		m_CommandUID = DayZPlayerConstants.CMD_ACTIONFB_MINEROCK;
		m_FullBody = true;
		m_StanceMask = DayZPlayerConstants.STANCEMASK_ERECT;
		m_SpecialtyWeight = UASoftSkillsWeight.ROUGH_HIGH;

		if (GetGame().IsServer()){
            miningConfig = P200MiningConfig.Cast(GetPlugin(P200MiningConfig));
        }
	}

	override void CreateConditionComponents()
	{
		m_ConditionTarget = new CCTBuilding(UAMaxDistances.DEFAULT);
		m_ConditionItem = new CCINonRuined;
	}

	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		EntityAI targetObject = EntityAI.Cast(target.GetObject());
		if (targetObject)
		{
			map<string, string> testMap = g_P200MiningClientConfig;
            string itemType = targetObject.GetType();
			if(g_P200MiningClientConfig.Contains(itemType)) {
			    this.actionText = g_P200MiningClientConfig.Get(itemType);
				return true;
			}
		}
		return false;
	}

	override string GetText()
	{
		return this.actionText;
	}

	override void OnFinishProgressServer( ActionData action_data ) {
	    action_data.m_Player.GetSoftSkillsManager().AddSpecialty( m_SpecialtyWeight );
	    EntityAI targetObject = action_data.m_Target.GetObject();

	    if (targetObject){
			int count = 1;
			if(miningConfig.mineCount.Get(targetObject.ToString())) {
				count = count + miningConfig.mineCount.Get(targetObject.ToString()); 
			}
			miningConfig.mineCount.Set(targetObject.ToString(), count);
	        ref P200MinableItemConfig config = miningConfig.getItemConfig(targetObject.GetType());
	        if(config != null && config.max_mine > -1 && config.max_mine <= miningConfig.mineCount.Get(targetObject.ToString()) ) {
       	        GetGame().ObjectDelete( targetObject );
	        } else {
	            // Print("Item not deleted");
	        }
	    }
	}
};