// Config
static ref array<ref P200ActionConfig> g_P200ActionsUnlimitedClientConfig = {};

static ref array<ref P200ActionConfig> g_getP200UnlimitedActions(EntityAI target, EntityAI tool, ref array<ref P200ActionConfig> actions ) {
    string targetType;
    string toolType;
    ref array<ref P200ActionConfig> result = {};
    if(target){
        targetType = target.GetType();
    }
    if(tool) {
        toolType = tool.GetType();
    }
    for(int i = 0; i < actions.Count(); i++)
    {
        P200ActionConfig config = actions.Get(i);
        if(config.hasType(targetType, toolType)) {
            result.Insert(config);
        }
    }

    if(result.Size() > 1) {
        Print("[P200_Actions_Unlimited] ERROR: more than 1 action match "+ toolType + " to "+ targetType);
    }

    return result;
}